<?php

namespace App\Http\Controllers;

use App\LicencePro;
use Illuminate\Http\Request;

class LicenceProController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lp = LicencePro::orderBy('city')->get();

        return view('licences-pro-special', compact('lp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $values = request()->validate([
            'title' => 'required|max:255' ,
            'city' => 'required|max:255' ,
            'link' => 'nullable|active_url|url',
            'details' => 'required' ,
            'description' => 'required'
        ]);

        LicencePro::create($values);

        return redirect('/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LicencePro  $licencePro
     * @return \Illuminate\Http\Response
     */
    public function show(LicencePro $licencePro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LicencePro  $licencePro
     * @return \Illuminate\Http\Response
     */
    public function edit(LicencePro $licencePro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LicencePro  $licencePro
     * @return \Illuminate\Http\Response
     */
    public function update(LicencePro $lp)
    {
        $lp->update(request()->validate([
            'title' => 'required|max:255' ,
            'city' => 'required|max:255' ,
            'link' => 'nullable|active_url|url',
            'details' => 'required' ,
            'description' => 'required'
        ]));
        

        return redirect('/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LicencePro  $licencePro
     * @return \Illuminate\Http\Response
     */
    public function destroy(LicencePro $lp)
    {
        $lp->delete();

        return redirect('/admin');
    }
}
