<?php

namespace App\Http\Controllers;

use App\Temoignage;
use Illuminate\Http\Request;

class TemoignageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show','index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $temoignages = Temoignage::orderBy('created_at', 'desc')->simplePaginate(15);
        return view('admin.temoignages.index', compact('temoignages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.temoignages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $values = request()->validate([
            'author' => 'required|string|max:100',
            'author_info' => 'required|string|max:255',
            'title' => 'required|string|max:100' ,
            'description' => 'required|string'
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $path = $request->image->store('image', 'public');
            $values['image'] = $path;
        }
        // else {
        //     dd($request->file('image'));
        // }

        Temoignage::create($values);

        return redirect('/admin/temoignages')->with('status', 'create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Temoignage  $temoignage
     * @return \Illuminate\Http\Response
     */
    public function show(Temoignage $temoignage)
    {
        return view('admin.temoignages.show',compact('temoignage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Temoignage  $temoignage
     * @return \Illuminate\Http\Response
     */
    public function edit(Temoignage $temoignage)
    {
        return view('admin.temoignages.edit', compact('temoignage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Temoignage  $temoignage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Temoignage $temoignage)
    {

        $values = request()->validate([
            'author' => 'required|string|max:100',
            'author_info' => 'required|string|max:255',
            'title' => 'required|string|max:100' ,
            'description' => 'required|string'
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $path = $request->image->store('image', 'public');
            $values['image'] = $path;
        } 
        // else {
        //     dd($request->file('image'));
        // }

        $temoignage->update($values);

        return redirect('/admin/temoignages')->with('status', 'modif');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Temoignage  $temoignage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Temoignage $temoignage)
    {
        $temoignage->delete();

        return redirect('/admin/temoignages');
    }

    public function hide(int $id)
    {
        $temoignage = Temoignage::findOrFail($id);
        $temoignage->visible = false;
        $temoignage->update();


        return redirect('/temoignages');
    }

    public function toggleVisibility(int $id)
    {
        $temoignage = Temoignage::findOrFail($id);
        $temoignage->visible = ! $temoignage->visible ;
        $temoignage->update();


        return redirect('/admin/temoignages');
    }


}
