<?php

namespace App\Http\Controllers;

use App\Announce;
use Illuminate\Http\Request;

class AnnounceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show','index', 'store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announces = Announce::orderBy('created_at' , 'desc')->simplePaginate(15);
        return view('admin.announces.index', compact('announces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.announces.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $values = request()->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:4000',
            'type' => 'required',
            'date_beginning' => 'required',
            'minimum_duration' => 'required',

            'company_name' => 'required|string|max:255',
            'company_country' => 'required|string|max:255',
            'company_city' => 'required|string|max:255',
            'company_address' => 'nullable|string|max:255',
            'company_zipcode' => 'nullable|string|max:255',

            'contact_firstname' => 'nullable|string',
            'contact_lastname' => 'nullable|string',
            'contact_phone' => 'nullable|string',
            'contact_email' => 'nullable|string',
        ]);

        Announce::create($values);

        return redirect('/annonces')->with('status', "Elle apparaîtra sur le site si la modération la valide.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Announce  $stage
     * @return \Illuminate\Http\Response
     */
    public function show(Announce $stage)
    {
        return view('admin.announces.show',compact('stage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Announce  $stage
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stage = Announce::find($id);
        return view('admin.announces.edit', compact('stage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Announce  $stage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announce $stage)
    {
        $stage->update(request()->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:4000',
            'type' => 'required',
            'date_beginning' => 'required',
            'minimum_duration' => 'required',

            'company_name' => 'required|string|max:255',
            'company_country' => 'required|string|max:255',
            'company_city' => 'required|string|max:255',
            'company_address' => 'nullable|string|max:255',
            'company_zipcode' => 'nullable|string|max:255',

            'contact_firstname' => 'nullable|string',
            'contact_lastname' => 'nullable|string',
            'contact_phone' => 'nullable|string',
            'contact_email' => 'nullable|string',
        ]));

        return redirect('/admin/stages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Announce  $stage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announce $stage)
    {
        $stage->delete();

        return redirect('/admin/stages');
    }

    public function toggleVisibility(int $id)
    {
        $stage = Announce::findOrFail($id);
        $stage->validated = true;
        $stage->visible = ! $stage->visible ;
        $stage->update();

        return redirect('/admin/stages');
    }
}
