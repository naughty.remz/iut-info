<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Announce;
use App\Temoignage;
use App\User;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.index');
    }

    public function articles()
    {
        $articles = Article::orderBy('created_at','desc')->get();
        return view('admin.articles', compact('articles'));
    }

    public function stages()
    {
        $stages = Announce::orderBy('created_at','desc')->get();
        return view('admin.stages', compact('stages'));
    }

    public function temoignages()
    {
        $temoignages = Temoignage::orderBy('created_at','desc')->get();
        return view('admin.temoignages', compact('temoignages'));
    }

    public function users()
    {
        $users = User::orderBy('created_at','desc')->get();
        return view('admin.users', compact('users'));
    }
}
