<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LicencePro extends Model
{
    protected $fillable = [
        'title',
        'city',
        'link',
        'details',
        'description',
        'type'
    ];
}
