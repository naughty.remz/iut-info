# Guide d'installation 

Ce document constitue la documentation d'installation du projet *"refonte du portail des iut informatique"* réalisé par le **Crew Core** (Equipe de Toulouse)  
> Pour toutes questions ou problèmes lors de l'installation, contactez-nous sans hésiter !  
> **crewcore31@gmail.com**

## Prérequis pour l'installation

L'installation nécessite que les composants suivants soient installés sur la machine :

* PHP 7.2 : [http://php.net/downloads.php](http://php.net/downloads.php)
* Composer : [https://getcomposer.org/download/](https://getcomposer.org/download/)
* Node JS et l'utilitaire npm : [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
* MySQL : [https://www.mysql.com/fr/](https://www.mysql.com/fr/)
* Un serveur Apache ou nginx

## Installer le projet

1. Utiliser un terminal et cloner le repo avec 

```sh
$ git clone adresseDuRepo
```
dans le dossier /www

2. Rendez-vous à la racine du projet :
```sh
$ cd iut-info/
```

3. Taper la commande :
```sh
$ composer install
```

4. Renommer le fichier `.env.exemple` en `.env` avec la commande 
```sh
$ cp .env.example .env
```

5. Taper 
```sh
$ php artisan key:generate
```

## Base de données 

1. Créez d'abord une base de donnée (vide) **mysql** en schéma  **utf8_general_ci**   
2. Configurer le fichier .env pour donner au site l'accès à une base de donnée
```env
...

APP_ENV=production

...

DB_DATABASE= nomBDD
DB_USERNAME= nomUtilisateur
DB_PASSWORD= mdp

...

```
Les identifiants fournis doivent correspondre à un utilisateur ayant les droits de lecture et d'écriture sur la base

3. Il faut ensuite remplir la base de données :

```sh
$ php artisan migrate:fresh
```

```sh
$ php artisan db:seed
```

## Quelques commandes supplémentaires

```sh
$ php artisan storage:link
```

```sh
$ npm install
```

```sh
$ npm run prod
```


Créer un virtual host qui pointe vers le dossier **/public**


#### Installation terminée ✅
Login de l'administrateur : **concours.site@email.fr**  
Mot de passe : **$iutinfoconcours**  
Adresse de la partie admin : **http:// ... /admin**  
