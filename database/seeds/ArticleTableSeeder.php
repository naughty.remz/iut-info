<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
	        DB::table('articles')->insert([
	            'title' => $faker->name,
	            'photo' => $faker->image('image',400,300, null, false),
	            'description' => text($maxNbChars = 200)   ,
	        ]);
	    }
    }
}
