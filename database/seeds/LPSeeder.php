<?php

use Illuminate\Database\Seeder;
use App\LicencePro;

class LPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('licence_pros')->delete();

        $json = File::get("database/metierDuNet.json");
        $data = json_decode($json);
        foreach($data as $obj){
            LicencePro::create(array(
                'title' => $obj->title,
                'city' => $obj->city,
                'link' => $obj->link,
                'details' => $obj->details,
                'description' => $obj->description,
                'type' => "MN"
            ));
        }

        $json = File::get("database/sysReseaux.json");
        $data = json_decode($json);
        foreach($data as $obj){
            LicencePro::create(array(
                'title' => $obj->title,
                'city' => $obj->city,
                'link' => $obj->link,
                'details' => $obj->details,
                'description' => $obj->description,
                'type' => "SR"
            ));
        }

        $json = File::get("database/informatiqueEmbarquee.json");
        $data = json_decode($json);
        foreach($data as $obj){
            LicencePro::create(array(
                'title' => $obj->title,
                'city' => $obj->city,
                'link' => $obj->link,
                'details' => $obj->details,
                'description' => $obj->description,
                'type' => "IE"
            ));
        }

        $json = File::get("database/imageEtSonNumerique.json");
        $data = json_decode($json);
        foreach($data as $obj){
            LicencePro::create(array(
                'title' => $obj->title,
                'city' => $obj->city,
                'link' => $obj->link,
                'details' => $obj->details,
                'description' => $obj->description,
                'type' => "ISN"
            ));
        }

        $json = File::get("database/genieLogicielSystemesdInformation.json");
        $data = json_decode($json);
        foreach($data as $obj){
            LicencePro::create(array(
                'title' => $obj->title,
                'city' => $obj->city,
                'link' => $obj->link,
                'details' => $obj->details,
                'description' => $obj->description,
                'type' => "GLSI"
            ));
        }
    }
}
