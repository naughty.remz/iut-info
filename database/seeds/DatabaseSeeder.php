<?php

use Illuminate\Database\Seeder;
use App\Article;
use App\Announce;
use App\Temoignage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // Random content seed
        factory(Article::class, 12,'fr_FR')->create();
        factory(Temoignage::class, 13,'fr_FR')->create();
        factory(Announce::class, 15,'fr_FR')->create();


        // Creation des licences pro depuis un fichier Json
        $this->call(LPSeeder::class);


        // Creation des comptes admin
        DB::table('users')->insert([
            'name' => 'Administrateur',
            'email' => 'concours.site@email.fr',
            'password' => bcrypt('$iutinfoconcours'),
            'type' => 'admin',
        ]);
        DB::table('users')->insert([
            'name' => 'CrewCore',
            'email' => 'crewcore31@gmail.com',
            'password' => bcrypt('Yoloswag69'),
            'type' => 'admin',
        ]);
    }


}
