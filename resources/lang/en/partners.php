<?php

return[
    
    'header' => "Partners",

    'title' => "Partner companies",
    
    'intro' => "The computer science departments of IUTs are partners of large computer companies. This allows students to integrate these companies more easily at the end of their training, through the end-of-year internship.",

    'listIntro' => "Among these partners are:",

    'internationalTitle' => "International partnerships",

    'internationalIntro' => "In addition to professional partnerships, IT departments in partnership with foreign universities welcome foreign students to train them in computer science. But also by sending students to do their internship abroad.",

    'internationalListIntro' => "We find for example:",
	

];