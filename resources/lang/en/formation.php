<?php

return[

	'header' => "Education",

    'intro' => "Introduction",
    
    'nbIut' => "universities in France",

    'sentence1' => "The department of computer science of the IUT in France train students to computing and to team work so as to prepare them to their integration in working life.",

    'sentence2' => "It therefore offers a  teaching supervised by available teachers, and professionals who teach students a practical and concrete experience of the computing line of work.",

    'analyseTitle' => "Analysis </br> &amp; design",
    'analyseDesc' => "That deals with the previous work in relation with the customer to specify his needs and expectations.",

    'algoTitle' => "Algorithmic &amp; programming",
    'algoDesc' => "Mise en oeuvre effective du traitement jusqu'au codage. Esprit logique, précision technique",

    'archiTitle' => "Architecture système &amp; réseaux",
    'archiDesc' => "the reflexion on the solution to give to the customer and its encoding (Internet, Intranet, ...).",

    'ajouts' => "To those teaching objectives, other general academic disciplines are added",

    'math' => "Mathematics",
    'eco' => "Economy",
    'gest' => "Management",
    'ang' => "English",
    'com' => "Communication",

    'lastText' => "Besides, the assessment is based on theory but also on practical situations, via supervised projects and a work placement in a company enabling to validate the knowledge and skills acquired during the training.",


    
    'prog' => "official program",

    'fr' => "French version",
    'de' => "Deutch version",
    'es' => "Spanish version",


];