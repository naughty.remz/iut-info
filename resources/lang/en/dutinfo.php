<?php

return[
	
    'header' => "IT Technology Degree",
    
    'intro' => "Introduction",

    'introtext' => "This DUT trains assistant engineers and project managers in computer management and industrial computing. Immediately operational in software and hardware development, they participate in the design, implementation and implementation of computer systems according to the specifications that are submitted to them. They are able to meet the needs of companies in terms of network administration, program design and implementation, technical assistance, database management ...",

    'jobexample' => "Jobs examples :",
    
    'job1' => "Analyst Programmer",

    'job2' => "Mobile application developer",

    'job3' => "Database specialist",

    'job4' => "Industrial computer scientist",

    'job5' => "Web Integrator",

    'job6' => "Computer maintenance technician",

    'job7' => "Computer tester",

    'job8' => "Project Manager",

    'jobothers' => "and many more ...",

    'access' => "Access to Training",

    'accesstext' => "Be a holder of a general (primarily scientific), technological or professional bachelor's degree or pass a VAE (validation of the acquired experience or personal studies). The holders of baccalaureate S and SI are the most represented in this training.",

    'accesstext2' => "Students are chosen based on their profile, and sometimes as a result of a test and / or interview. It is also possible to go directly to the second year (to do your DUT in 1 year) if the candidate has validated 60 ECTS credits or followed a higher education of 2 years.",

    'discover' => "Discover the content of the training",

];