<?php

return[

	'home' => 'Liste des annonces de stage / apprentissage',

	'create' => 'Nouvelle offre de stage',

    'edit' => "Édition d'une offre de stage",

    'search' => "Vous recherchez un <span class=\"stage\">stagiaire</span> ou un <span class=\"apprentissage\">étudiant en apprentissage</span> ?",

    'click' => "Proposez une annonce",

];
