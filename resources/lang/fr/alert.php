<?php

return[
	
	'succes' => 'Succès',

	'articleModified' => 'Article modifié !',

	'articleCreated' => 'Article créé !',

	'testimonialModified' => 'Témoignage modifié !',

    'testimonialCreated' => 'Témoignage créé !',
    
    'jobModified' => 'Offre de stage modifié !',

	'jobCreated' => 'Offre de stage créé !',

];