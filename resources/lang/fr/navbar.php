<?php

return[

	'home' => "Accueil",

	'formations' => "Les formations",

	'formation' => "Formation",

	'about' => "À Propos de l'IUT",

	'IUTInFr' => "Les IUT en France",

	'pres' => "Présentation",

	'tem' => "Témoignages",

	'companies' => "Entreprises" ,

	'after' => "Après l'IUT",

	'part' => "Partenaires",

	'offers' => "Offres de stages",

	'deb' => "Débouchés",

	'studies' => "Poursuite d'étude",

	'dut' => "Les DUT Informatiques",

	'lp' => "Les Licences professionnelles",

	'news' => "Actualités"

	
];
