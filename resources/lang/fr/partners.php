<?php

return[

	'header' => "Partenaires",

    'title' => "Les entreprises partenaires",
    
    'intro' => "Les départements d'informatique des IUT ont pour partenaires de grandes sociétés d'informatique. Ce qui permet aux étudiants d'intégrer ces sociétés plus facilement à la fin de leur formation, via le stage de fin d'année.",

    'listIntro' => "On y retrouve par exemple :",

    'internationalTitle' => "Partenariats internationaux",

    'internationalIntro' => "En plus des partenariats professionnels, les départements d'informatique en partenariat avec des universités étrangères accueillent des étudiants étrangers pour les former à l'informatique. Mais aussi en envoyant les étudiants effectuer leur stage à l'étranger.",

    'internationalListIntro' => "On retrouve par exemple :",

];