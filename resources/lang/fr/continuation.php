<?php

return[

	'title' =>"La vocation première de l'IUT est de mener directement au monde professionnel, mais il est possible de poursuivre ses études si l'étudiant le souhaite réellement.",

	'after' => "Après le DUT:",

	'l1'=>"Ecoles d'ingénieurs (ENSIMAG, ISIM, ENSEIHT, IIE, ENSSAT, INT, INSA, IFIPS, UTC, ...)",

	'l2'=>"Licence et maîtrise d'informatique",

	'l3'=>"Licence et maîtrise de sciences et techniques informatiques",

	'l4'=>"IUP MIAGE (maîtrise des méthodes informatiques appliquées à la gestion)",

	'l5'=>"Formation complémentaires à l'étranger",

	'l6'=>"Formation en alternance (préparation au DEST)",

	'l7'=>"Master informatique",

	'l8'=>"Licence professionnelle systèmes informatiques et logiciels",

	'button' => "En savoir plus sur les licences professionnelles"

];