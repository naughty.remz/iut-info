<?php

return[
	
	't1'=> 'Production et exploitation',

	'p1' => "Administrateur de bases de données, Administrateur systèmes et réseaux, Analyste d'exploitation",

	'p2' => "Gestionnaire de parc micro, Technicien help desk, Technicien réseaux et télécoms",

	'p3' => "Webdesigner, Webmaster.",

	't2'=>'Etudes, développement et intégration',

	'p4' => "Chefs de projet en SSII ou en TIC, Directeur de projet",

	'p5' => "Développeur de jeux vidéo multimédia, Analyste programmeur, Intégrateur, Paramétreur de PGI.",

	't3'=>'Administration et gestion de la DSI',

	'p6' => "Auditeur informatique",

	'p7' => "Directeur des systèmes d'information, Responsable des études, Responsable d'exploitation.",

	't4'=>'Support et assistance technique',

	'p8' => "Chargé d'affaires interne, Formateur, Analyste système, Directeur technique, Responsable sécurité",

	'p9' => "Spécialiste sécurité, Spécialiste méthode",

	'p10' => "Architecte réseaux, Architecte technique, Architecte base de données.",

	't5'=>"Conseil en système d'information et maîtrise d'ouvrage (MOA)",

	'p11' => "Chef de projet MOA, Conseil - consultant en système d'information , Consultant en business intelligence, Consultant en sécurité, Consultant MOA , Consultant ERP , Consultant spécialisé sur des marchés non encore matures",

	'p12' => "Responsable en conduite du changement, Responsable de SI métier, Responsable de projet fonctionnel",

	'p13' => "Architecte du SI, Correspondant informatique.",




];