import Granim from 'granim';
import Typed from 'typed.js';

$(function () {
    $('.carousel').removeClass("hidden");
    $('.carousel').slick({
        dots: true,
        infinite: false,
        variableWidth: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        speed: 500,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 8000,

        // prevArrow: "<img class='a-left reverse control-c prev slick-prev' src='img/arrow_white.png' >",
        // nextArrow: "<img class='a-right control-c next slick-next' src='img/arrow_white.png' >",
        responsive: [
            {
                breakpoint: 1450,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 666,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    new Granim({
        element: '#canvas-presentation',
        direction: 'diagonal',
        isPausedWhenNotInView: true,
        image: {
            source: './img/business-businessmen-classroom-267507.jpg',
            blendingMode: 'multiply',
            position: ['center', 'center'],
            stretchMode: ['stretch-if-smaller', 'none'],
        },
        states: {
            "default-state": {
                gradients: [
                    ['#69FBA8', '#35E3AE'],
                    ['#35E3AE', '#48ACD9'],
                    ['#48ACD9', '#5257E1'],
                    ['#5257E1', '#5449A1']
                ],
                transitionSpeed: 4000
            }
        }
    });

    new Typed("#typed", {
        stringsElement: '#typed-strings',
        typeSpeed: 10,
        backDelay: 6000,
        loop: true
    });

    $('.testimonial-description').hide();

    $('.carousel-element').hover(function () {
        $(this).find('.testimonial-description').stop().animate({
            height: "toggle",
            opacity: "toggle",
            'padding-bottom': "toggle"
        }, 270);
        $(this).find('.testimonial-text').stop().animate({
            height: "toggle",
            opacity: "toggle"
        }, 270);
    });
});
