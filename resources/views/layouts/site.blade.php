<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="theme-color" content="#2e90f6">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <title>
        @if (View::hasSection('title'))
            @yield('title') - Portail National IUT Informatique 
        @else
            Portail National IUT Informatique
        @endif
    </title>
    <link rel="icon" sizes="192x192" href="img/favicon.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome/all.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/reset.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/aos.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/hero.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/partenaires.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/formation.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/terminal.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/footer.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bounce.css') }}">--}}

    @yield('head')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" type="text/css" href="{{ mix('dist/css/app.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/carousel.css') }}"> --}}

</head>
<!-- TYPE 'BOUNCE' FOR A SECRET EASTER EGG !!! -->
<body>
    @include('site.partials.navbar')
    <div class="main-container">
        @yield('content')
        @include('site.partials.footer')
    </div>
    <!-- JS -->
    <script src="{{ mix('dist/js/manifest.js') }}"></script>
    <script src="{{ mix('dist/js/vendor.js') }}"></script>
    <script src="{{ mix('dist/js/app.js') }}"></script>
    @yield('javascript')
</body>
</html>
