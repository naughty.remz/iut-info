@extends('layouts.admin')

@section('title', trans("testimonial.home"))

@section('content')
    <div class="header">
        <h1 class="title">Gestion des témoignages</h1>
        <a class="button is-link" href="/admin/temoignages/create">Ajouter un témoignage</a>
    </div>

    <div class="section">
        <table class="table is-fullwidth is-striped">
            <thead>
            <tr>
                <th>Auteur</th>
                <th>Date création</th>
                <th>Statut</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($temoignages as $temoignage)
                <tr>
                    <td>{{ $temoignage->author }}</td>
                    <td>{{ $temoignage->created_at }}</td>
                    <td class="{{ $temoignage->visible ? 'has-text-success' : 'has-text-danger' }}">{{ $temoignage->visible ? 'Visible' : 'Invisible' }}</td>
                    <td>
                        <div class=" buttons has-addons">
                            <a class="button is-info is-small" href="{{ url("admin/temoignages/$temoignage->id/edit") }}">Editer</a>
                            <a class="button is-warning is-small" href="{{ route('temoignages.toggleVisibility', ['id' => $temoignage->id]) }}">{{ $temoignage->visible ? 'Cacher' : 'Afficher' }}</a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $temoignages->links('vendor.pagination.default', ['elements' => $temoignages])  }}
    </div>
@endsection
