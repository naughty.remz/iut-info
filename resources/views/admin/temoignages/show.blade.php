@extends('layouts.admin')

@section('title', $temoignage->title )

@section('content')

    <div class="testimonial-show">
        <div class="testimonial-infos">
            <div class="back-testimonial">
                <canvas id="canvas-testimonial"></canvas>
            </div>
            <div class="front-testimonial">
                @if( !is_null($temoignage->image) )
                <div
                    class="image-testimonial"
                    style="background:url({{ $temoignage->image }});background-size: cover; background-position:center;"></div>
                @else
                <div
                    class="image-testimonial"
                    style="background:url({{
                        asset('img/user_placeholder.png')
                    }});background-size: cover; background-position:center;"
                ></div>
                @endif

                <h1>{{ $temoignage->title }}</h1>
                <h5 class="temoigneur"> Par {{ $temoignage->author }}</h5>           
            </div>
        </div>
        <div class="testimonial-content-show">
            <div class="quote-testimonial-show">
                <img src="{{ asset('img/quote.png') }}">
            </div>
            <p class="temoignageContenu">{!! nl2br(e( $temoignage->description )) !!}</p>
        </div>

        <div class="button-feed-container temoignages-button temoignageBouton">
            <a href="/temoignages">
                <button>Tous les témoignages</button>
             </a>
        </div>

    </div>

    <script type="text/javascript">
        var granimInstance = new Granim({
            element: '#canvas-testimonial',
            direction: 'top-bottom',
            isPausedWhenNotInView: true,
            states : {
                "default-state": {
                    gradients: [
                        ['#69FBA8', '#35E3AE'],
                        ['#35E3AE', '#48ACD9'],
                        ['#48ACD9', '#5257E1'],
                        ['#5257E1', '#5449A1']
                    ],
                    transitionSpeed: 3000
                }
            }
        });           
    </script>

@endsection
