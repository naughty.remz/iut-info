@extends('layouts.admin')

@section('title', trans("testimonial.create"))

@section('content')
    <div class="admin-create-container">
        <div class="admin-create-content">

            <h1 class="title">Création d'un témoignage</h1>

            <form method="POST" action="/admin/temoignages" enctype='multipart/form-data'>
                {{ csrf_field() }}
                {{ method_field('POST') }}

                <div class="field">
                    <label class="label" for="">Nom de l'auteur</label>
                    <input class="input" type="text" name="author" placeholder="Nom de l'auteur du témoignage" required
                           value="{{ old('author') }}" maxlength="100">
                </div>
                <div class="field">
                    <label for="">Infos sur auteur</label>
                    <input class="input" type="text" name="author_info" placeholder="Informations sur l'auteur" required
                           value="{{ old('author_info') }}" maxlength="255">
                </div>
                <div class="field">
                    <label for="">Photo de l'auteur</label>
                    <div class="file has-name">
                        <label class="file-label">
                            <input class="file-input" type="file" name="image" id="author-photo">
                            <span class="file-cta">
                              <span class="file-icon">
                                <i class="fas fa-upload"></i>
                              </span>
                              <span class="file-label">
                                Choisissez un fichier…
                              </span>
                            </span>
                            <span class="file-name" id="filename">
                            </span>
                        </label>
                    </div>
                </div>

                <div class="field">
                    <label for="">Titre du témoignage</label>
                    <input class="input" type="text" name="title" placeholder="Titre" required
                           value="{{ old('title') }}"
                           maxlength="100">
                </div>
                <div class="field">
                    <label for="">Contenu du témoignage</label>
                    <textarea class="textarea" type="text"
                              placeholder="Description"
                              name="description"
                              value="{{ old('description') }}"
                              required></textarea>
                </div>
                <input class="button is-primary" type="submit" name="ok" value="Créer le témoignage">
            </form>

            @include('admin.partials.errors')

        </div>
    </div>
@endsection

@section('javascript')
    <script>
        var file = document.getElementById("author-photo");
        file.onchange = function(){
            if (file.files.length > 0) {
                document.getElementById('filename').innerHTML = file.files[0].name;
            }
        };
    </script>
@endsection
