@if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
            <li class="error-list"><i class="fas fa-exclamation-triangle"></i> {{ $error }}</li>
        @endforeach
    </ul>
@endif