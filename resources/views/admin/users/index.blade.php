@extends('layouts.admin')

@section('content')
    <div class="header">
        <h1 class="title">Gestion des utilisateurs</h1>
        <a href="/admin/users/create" class="button is-link">Ajouter un utilisateur</a>
    </div>

    <div class="section">
        <table class="table is-striped is-hoverable is-fullwidth">
            <thead>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Date de création</th>
                <th>Rôle</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ str_limit($user->name, $limit = 30, $end = '...') }}</td>
                    <td>{{ str_limit($user->email, $limit = 80, $end = '...') }}</td>
                    <td>{{ $user->updated_at }}</td>
                    @if ($user->type == 'admin')
                        <td class="has-text-success">Administrateur</td>
                    @endif
                    @if ($user->type == 'default')
                        <td class="has-text-warning">Modérateur</td>
                    @endif
                    <td>
                        <div class=" buttons has-addons">
                            <a class="button is-info is-small" href="{{ url("admin/users/$user->id/edit") }}">Editer</a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->links('vendor.pagination.default', ['elements' => $users])  }}
    </div>
@endsection
