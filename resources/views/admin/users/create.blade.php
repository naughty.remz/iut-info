@extends('layouts.admin')

@section('title', trans("users.edit"))

@section('content')
    <div class="admin-create-container">
        <div class="admin-create-content">
            <h1 class="title">Modification d'un user</h1>

            <form method="POST" action="/admin/users">

                {{ csrf_field() }}

                {{ method_field('POST') }}

                <div class="field">
                    <label class="label">Nom</label>
                    <input class="input" type="text" name="name" value="{{ old('name') }}">
                </div>

                <div class="field">
                    <label class="label">Email</label>
                    <input class="input" type="email" name="email" value="{{ old('email ') }}">
                </div>

                <div class="field">
                    <label class="label">Type de compte</label>
                    <div class="control">
                        <label class="radio" for="type-admin">
                            <input value="admin" type="radio" id="type-admin" name="type" required  {{ old('type') == 'admin' ? 'checked' : ''  }}>
                            Admin
                        </label>
                        <label class="radio" for="type-default">
                            <input value="default" type="radio" id="type-default" name="type" required {{ old('type') == 'default' ? 'checked' : ''  }}>
                            Modérateur
                        </label>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Mot de passe</label>
                    <input class="input" type="password" name="password">
                </div>

                <div class="field">
                    <label class="label">Répétez le mot de passe</label>
                    <input class="input" type="password" name="password_confirmation">
                </div>

                <input class="button is-primary" type="submit" name="ok" value="Ajouter l'utilisateur">
            </form>

            @include('admin.partials.errors')
        </div>
    </div>
@endsection
