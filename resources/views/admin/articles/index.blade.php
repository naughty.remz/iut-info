@extends('layouts.admin')

@section('title', trans("news.home"))

@section('content')
    <div class="hearder">
        <h1 class="title">Gestion des news</h1>
        <a class="button is-link" href="/admin/articles/create">Ajouter un article</a>
    </div>

    <div class="section">
        <table class="table is-fullwidth is-striped">
            <thead>
            <tr>
                <th>Titre</th>
                <th>Date création</th>
                <th>Statut</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($articles as $article)
                <tr>
                    <td>{{ $article->title }}</td>
                    <td>{{ $article->created_at }}</td>
                    <td class="{{ $article->visible ? 'has-text-success' : 'has-text-danger' }}">{{ $article->visible ? 'Visible' : 'Invisible' }}</td>
                    <td>
                        <div class=" buttons has-addons">
                            <a class="button is-info is-small" href="{{ url("admin/articles/$article->id/edit") }}">Editer</a>
                            <a class="button is-warning is-small" href="{{ route('articles.toggleVisibility', ['id' => $article->id]) }}">
                                {{ $article->visible ? 'Cacher' : 'Afficher' }}
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $articles->links('vendor.pagination.default', ['elements' => $articles])  }}
    </div>
@endsection
