@extends('layouts.admin')

@section('title', trans("news.edit"))

@section('content')

<div class="admin-create-container">
    <div class="admin-create-content">
    <h1 class="title">Modification de l'article </h1>

    <form method="POST" action="/admin/articles/{{ $article->id }}" enctype='multipart/form-data'>

        {{ csrf_field() }}
        {{ method_field('PATCH') }}

        <div class="field">
            <label class="label" for="title">Titre</label>
            <input class="input" type="text" name="title" placeholder="Titre de l'article" required value="{{ $article->title }}" maxlength="100">
        </div>
        <div class="field">
            <label for="">Image de l'article</label>
            <div class="file has-name">
                <label class="file-label">
                    <input class="file-input" type="file" value="{{ $article->photo }}" name="image" id="photo">
                    <span class="file-cta">
                        <span class="file-icon">
                        <i class="fas fa-upload"></i>
                        </span>
                        <span class="file-label">
                        Choisissez un fichier…
                        </span>
                    </span>
                    <span class="file-name" id="filename">
                    </span>
                </label>
            </div>
        </div>
        <div class="field">
            <label class="label" for="description">Description</label>
            <textarea class="textarea" name="description" placeholder="Article" required value="{{ $article->description }}">{{ $article->description }}</textarea>
        </div>
        <div class="field">
            <label class="label" for="photo">Auteur</label>
            <input class="input" type="text" name="author" placeholder="Auteur" value="{{ $article->author }}">
        </div>
        <div class="field">
            <button class="button is-primary" type="submit">Modifier article</button>
        </div>
    </form>

    @if( auth()->user()->isAdmin() )
        <form method="POST" action="/admin/articles/{{ $article->id }}">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}

            <div>
                <div class="suppr-article">
                    <br>
                    <button class="button is-danger" type="submit">Supprimer l'article</button>
                </div>
            </div>
        </form>
    @endif

    @include('admin.partials.errors')

    </div>
</div>
@endsection
