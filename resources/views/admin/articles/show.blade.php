@extends('layouts.admin')

@section('title', $article->title)

@section('content')

        <div class="states-images">
            @if( !is_null($article->photo) )
            <div
                class="photo-article"
                style="background:url({{ $article->photo }});background-size: cover; background-position:center;">  
            </div>
            @endif
            <div class="back-article">
                <canvas id="canvas-image-blending"></canvas>
            </div>
            <div class="front-article">
                <div class="container-top-infos">
                    <h1>{{ $article->title }}</h1>
                    <div class="bar"></div>
                    <h5 class="temoigneur">Le {{ \Carbon\Carbon::parse($article->created_at)->format('d/m/Y à H:i')}}</h5>
                </div>
            </div>
        </div>
        <div class="container-article">
            <p class="articleContenu">{{ $article->description }}</p>

            <p class="article-author">Article écrit par {{ $article->author }}</p>
            <div class="button-feed-container temoignages-button temoignageBouton">
                <a href="/articles">
                    <button>Tous les articles</button>
                 </a>
            </div>
        </div>

        <script type="text/javascript">
            var granimInstance = new Granim({
                element: '#canvas-image-blending',
                direction: 'top-bottom',
                isPausedWhenNotInView: true,
                states : {
                    "default-state": {
                        gradients: [
                            ['#69FBA8', '#35E3AE'],
                            ['#35E3AE', '#48ACD9'],
                            ['#48ACD9', '#5257E1'],
                            ['#5257E1', '#5449A1']
                        ],
                        transitionSpeed: 3000
                    }
                }
            });           
        </script>
@endsection
