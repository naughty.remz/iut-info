@extends('layouts.admin')

@section('content')

<div class="section">
    <h1 class="title">Interface Administrateur</h1>

    <article class="message is-success">
        <div class="message-header">
            <p>Accueil</p>
        </div>
        <div class="message-body">
            Cet interface administrateur vous permet de <strong>créer, éditer et supprimer</strong> le contenu du site.
            Vous avez accès aux paramètres des <strong>Stages, Témoignages et Articles</strong>.
        </div>
    </article>
</div>


@endsection
