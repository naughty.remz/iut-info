@extends('layouts.admin')

@section('title', trans("internship.create"))

@section('content')
<div class="admin-create-container">
    <div class="admin-create-content">
    <h1>Création d'une offre de stage</h1>

    <form method="POST" action="/stages">

        {{ csrf_field() }}

        <input type="text" placeholder="Nom de l'entreprise" name="company_name" required value="{{ old('company_name') }}" maxlength="255">

        <input type="text" placeholder="Adresse de l'entreprise (optionnelle)" name="company_address" value="{{ old('company_address') }}" maxlength="255">

        <input type="tel" placeholder="Numéro de téléphone (optionnelle)" name="phone" value="{{ old('phone') }}" maxlength="20">

        <input type="email" placeholder="Adresse Email (optionnelle)" name="email" value="{{ old('email') }}" maxlength="255">

        <input type="text" placeholder="Maître de stage (optionnelle)" name="supervisor" value="{{ old('supervisor') }}" maxlength="255">

        <input type="text" placeholder="Titre" name="title" required value="{{ old('title') }}" maxlength="255">

        <textarea type="text" placeholder="Description" name="description" value="{{ old('description') }}">{{ old('description') }}</textarea>

        <p>Description</p>
        <textarea type="text" name="description" value="{{ old('description') }}" required>{{ old('description') }}</textarea>


        <input type="submit" name="ok" value="Créer l'offre de stage">
    </form>

    @if($errors->any())
        <ul>
            @foreach($errors->all() as $error)
                <li class="error-list"><i class="fas fa-exclamation-triangle"></i> {{ $error }}</li>
            @endforeach
        </ul>
    @endif
    </div>
</div>
    @include('admin.partials.errors')
    
@endsection
