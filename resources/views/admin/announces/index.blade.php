@extends('layouts.admin')

@section('title', trans("internship.home"))

@section('content')
    <div class="header">
        <h1 class="title">Gestion des annonces</h1>
    </div>

    <div class="section">
        <table class="table is-fullwidth is-striped">
            <thead>
            <tr>
                <th>Entreprise</th>
                <th>Titre</th>
                <th>Type</th>
                <th>Statut</th>
                <th>Date de création</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($announces as $announce)
                <tr>
                    <td>{{ $announce->company_name }}</td>
                    <td>{{ str_limit($announce->title, $limit = 20, $end = '...') }}</td>
                    <td>{{ $announce->type }}</td>
                    <td class="{{ $announce->visible ? 'has-text-success' : 'has-text-danger'}}" >
                        @if(!$announce->validated)
                            En attente de validation
                        @elseif(!$announce->visible)
                            Cachée
                        @else
                            Visible
                        @endif
                    </td>
                    <td>{{ $announce->created_at }}</td>
                    <td>
                        <div class=" buttons has-addons">
                            <a class="button is-info is-small" href="{{ url("admin/announces/$announce->id/edit") }}">Editer</a>
                            @if(!$announce->validated)
                                <a class="button is-warning is-small" href="{{ route('announces.toggleVisibility', ['id' => $announce->id]) }}">Valider</a>
                            @else
                                <a class="button is-warning is-small" href="{{ route('announces.toggleVisibility', ['id' => $announce->id]) }}">{{ $announce->visible ? 'Cacher' : 'Afficher' }}</a>
                            @endif
                        </div>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $announces->links('vendor.pagination.default', ['elements' => $announces])  }}
    </div>
@endsection
