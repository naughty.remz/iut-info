@extends('layouts.admin')

@section('title', trans("internship.edit"))

@section('content')
    <div class="admin-create-container">
        <div class="admin-create-content">
            <h1 class="title">Modification d'une offre de stage</h1>

            <form method="POST" action="/admin/stages/{{ $stage->id }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="columns">
                    <div class="column">
                        <h2 class="subtitle">Entreprise</h2>
                        <div class="field">
                            <label class="label" for="company_name">Raison sociale</label>
                            <input class="input" id="company_name" name="company_name" type="text" value="{{ $stage->company_name }}">
                        </div>
                        <div class="field">
                            <label class="label" for="company_country">Pays</label>
                            <input class="input" id="company_country" name="company_country" type="text" value="{{ $stage->company_country }}">
                        </div>
                        <div class="field">
                            <label class="label" for="company_zipcode">Code postal</label>
                            <input class="input" id="company_zipcode" name="company_zipcode" type="text" value="{{ $stage->company_zipcode }}">
                        </div>
                        <div class="field">
                            <label class="label" for="company_city">Ville</label>
                            <input class="input" id="company_city" name="company_city" type="text" value="{{ $stage->company_city }}">
                        </div>
                        <div class="field">
                            <label class="label" for="company_address">Adresse</label>
                            <input class="input" id="company_address" name="company_address" type="text" value="{{ $stage->company_address }}">
                        </div>
                    </div>
                    <div class="column">
                        <h2 class="subtitle">Contact dans l'entreprise</h2>
                        <div class="field">
                            <label class="label" for="contact_lastname">Nom</label>
                            <input class="input" id="contact_lastname" name="contact_lastname" type="text" value="{{ $stage->contact_firstname }}">
                        </div>
                        <div class="field">
                            <label class="label" for="contact_firstname">Prénom</label>
                            <input class="input" id="contact_firstname" name="contact_firstname" type="text" value="{{ $stage->contact_lastname }}">
                        </div>
                        <div class="field">
                            <label class="label" for="contact_phone">N° téléphone</label>
                            <input class="input" id="contact_phone" name="contact_phone" type="text" value="{{ $stage->contact_phone }}">
                        </div>
                        <div class="field">
                            <label class="label" for="contact_email">Email</label>
                            <input class="input" id="contact_email" name="contact_email" type="email" value="{{ $stage->contact_email }}">
                        </div>
                    </div>
                    <div class="column">
                        <h2 class="subtitle">Infos générales</h2>
                        <div class="field">
                            <label class="label" for="title">Intitulé du stage</label>
                            <input class="input" id="title" name="title" type="text" value="{{ $stage->title }}">
                        </div>
                        <div class="field">
                            <label class="label">Type d'annonce</label>
                            <div class="control">
                                <label class="radio" for="type-announce-stage">
                                    <input value="stage"
                                           type="radio"
                                           id="type-announce-stage"
                                           name="type"
                                           {{ $stage->type == 'stage' ? 'checked' : '' }}
                                           required>
                                    Stage
                                </label>
                                <label class="radio" for="type-announce-apprentissage">
                                    <input value="apprentissage"
                                           type="radio"
                                           id="type-announce-apprentissage"
                                           name="type"
                                           {{ $stage->type == 'apprentissage' ? 'checked' : '' }}
                                           required>
                                    Apprentissage
                                </label>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label" for="date_beginning">Date de début</label>
                            <input class="input" id="date_beginning" name="date_beginning" type="date" value="{{ $stage->date_beginning }}">
                        </div>
                        <div class="field">
                            <label class="label" for="minimum_duration">Durée minimum</label>
                            <input class="input" id="minimum_duration" name="minimum_duration" type="text" value="{{ $stage->minimum_duration }}">
                        </div>
                        <div class="field">
                            <label class="label" for="description">Description</label>
                            <textarea class="textarea" name="description" id="description">{{ $stage->description }}</textarea>
                        </div>
                    </div>
                </div>
                <div>
                    <input class="button is-primary" type="submit" name="ok" value="Valider les modifications">
                </div>
            </form>

            @if( auth()->user()->isAdmin() )
                <form method="POST" action="/admin/stages/{{ $stage->id }}">

                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}

                    <div>
                        <div class="suppr-article">
                            <br>
                            <button class="button is-danger" type="submit">Supprimer l'annonce</button>
                        </div>
                    </div>

                </form>
            @endif

            @if($errors->any())
                <ul>
                    @foreach($errors->all() as $error)
                        <li class="error-list"><i class="fas fa-exclamation-triangle"></i> {{ $error }}</li>
                    @endforeach
                </ul>
            @endif

        </div>
    </div>

    @include('admin.partials.errors')
@endsection
