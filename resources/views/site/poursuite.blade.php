@extends ('layouts.site')

@section('title', "Poursuite d'études")

@section('content')
    <div class="header-connexe">
        <div class="title-accueil">
            <h1>@lang('navbar.studies')</h1>
        </div>
    </div>

    <div class="contentContainer">

        <div class="contentSubcontainer">
            <p class="typicalText wrapper">@lang('continuation.title')</p>

            <p class="typicalSubtitle wrapper">@lang('continuation.after')</p>
            <ul class="typicalText wrapperList">
                <li>@lang('continuation.l1')</li>
                <li>@lang('continuation.l2')</li>
                <li>@lang('continuation.l3')</li>
                <li>@lang('continuation.l4')</li>
                <li>@lang('continuation.l5')</li>
                <li>@lang('continuation.l6')</li>
                <li>@lang('continuation.l7')</li>
                <li>@lang('continuation.l8')</li>
            </ul>
            <div data-aos="fade-left" data-aos-delay="150" class="typical-button-holder">
                <a href="/licences-pro">@lang('continuation.button')</a>
            </div>
        </div>
    </div>
@endsection


@section('javascript')
    <script>
        $(function(){
                $("#apres").addClass("open");
                $("#poursuite").addClass("here");
            });
    </script>
@endsection