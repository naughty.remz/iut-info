@extends ('layouts.site')

@section('title', 'Débouchés')

@section('content')

		<div class="header-connexe">
			<div class="title-accueil">
				<h1>@lang('navbar.deb')</h1>
			</div>
        </div>
        
        <div class="contentContainer">
            <div class="contentSubcontainer">
                <p class="typicalSubtitle wrapper">@lang('opportunities.t1')</p>
                <div class="imageText wrapper">
                    <ul class="typicalText wrapperList" data-aos="fade-zoom-in">
                        <li>- @lang('opportunities.p1')</li>
                        <li>- @lang('opportunities.p2')</li>
                        <li>- @lang('opportunities.p3')</li>
                    </ul>
                    <img data-aos="fade-left" data-aos-delay="400" src="{{ asset('img/icons/undraw_programmer_imem.svg') }}" alt=""/>
                </div>
            </div>

            <div class="contentSubcontainer">
                <p class="typicalSubtitle wrapper">@lang('opportunities.t2')</p>
                <div class="imageText wrapper">
                    <ul class="typicalText wrapperList" data-aos="fade-zoom-in">
                        <li>- @lang('opportunities.p4')</li>
                        <li>- @lang('opportunities.p5')</li>
                    </ul>
                    <img data-aos="fade-left" data-aos-delay="400" src="{{ asset('img/icons/undraw_programming_2svr.svg') }}" alt=""/>
                </div>
            </div>

            <div class="contentSubcontainer">
                <p class="typicalSubtitle wrapper">@lang('opportunities.t3')</p>
                <div class="imageText wrapper">
                    <ul class="typicalText wrapperList" data-aos="fade-zoom-in">
                        <li>- @lang('opportunities.p6')</li>
                        <li>- @lang('opportunities.p7')</li>
                    </ul>
                    <img data-aos="fade-left" data-aos-delay="400" src="{{ asset('img/icons/undraw_server_q2pb.svg') }}" alt=""/>
                </div>
            </div>

            <div class="contentSubcontainer">
                <p class="typicalSubtitle wrapper">@lang('opportunities.t4')</p>
                <div class="imageText wrapper">
                    <ul class="typicalText wrapperList" data-aos="fade-zoom-in">
                        <li>- @lang('opportunities.p8')</li>
                        <li>- @lang('opportunities.p9')</li>
                        <li>- @lang('opportunities.p10')</li>
                    </ul>
                    <img data-aos="fade-left" data-aos-delay="400" src="{{ asset('img/icons/undraw_broadcast_jhwx.svg') }}" alt=""/>
                </div>
            </div>

            <div class="contentSubcontainer">
                <p class="typicalSubtitle wrapper">@lang('opportunities.t5')</p>
                <div class="imageText wrapper">
                    <ul class="typicalText wrapperList" data-aos="fade-zoom-in">
                        <li>- @lang('opportunities.p11')</li>
                        <li>- @lang('opportunities.p12')</li>
                        <li>- @lang('opportunities.p13')</li>
                    </ul>
                    <img data-aos="fade-left" data-aos-delay="400" src="{{ asset('img/icons/undraw_wireframing_nxyi.svg') }}" alt=""/>
                </div>
            </div>

        </div>
        
@endsection

@section('javascript')
    <script>
        $(function(){
                $("#apres").addClass("open");
                $("#debouche").addClass("here");
            });
    </script>
@endsection