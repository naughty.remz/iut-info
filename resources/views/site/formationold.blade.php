@extends ('layouts.site')

@section('content')
	   <link rel="stylesheet" type="text/css" href="{{asset('css/odometer-theme-default.css')}}">

		<div class="header-connexe">
			<div class="title-accueil">
				<h1>La formation</h1>
			</div>
		</div>
        
        <h1 class="carousel-header connexe-title">L'I.U.T. c'est </h1>

        <div class="home-feed">

            <div class="content-stats">
                <div class="special-stats">
                    <div class="content-odometer"><h1 class="odometer odometer1"></h1></div>
                    <div class="content-stats">
                        <p>I.U.T. en France</p>
                    </div>
                </div>
                <div class="special-stats">
                    <div class="content-odometer"><h1 class="odometer odometer2"></h1></div>
                    <div class="content-stats">
                        <p>Ans d'existence</p>
                    </div>
                </div>
            </div>

            <div class="formation-subtitle">
                <h3>La force et le succès de l'I.U.T. en 3 points :</h3>
            </div>

            <div class="formation-flex">
                <div class="axe-formation" data-aos="fade-up" data-aos-delay="200">
                    <div class="number-axe-formation">
                        <h1>1</h1>
                    </div>
                    <p>Une formation qui couvre la plupart des secteurs</p>
                </div>
                <div class="axe-formation" data-aos="fade-up" data-aos-delay="500">
                    <div class="number-axe-formation">
                        <h1>2</h1>
                    </div>
                    <p>La collaboration de profesionnel(le)s</p>
                </div>
                <div class="axe-formation" data-aos="fade-up" data-aos-delay="800">
                    <div class="number-axe-formation">
                        <h1>3</h1>
                    </div>
                    <p>Des enseignant(e)s chercheurs/euses</p>
                </div>
            </div>
        </div>
            <div class="side-formation" data-aos="fade-up">
                <div class="left-side-formation image-formation">
                    <h1>6</h1>
                    <h3>mois</h3>
                </div>
                <div class="right-side-formation large-text">
                    <p>C'est le temps moyen qu'il faut à un(e) étudiant(e) pour trouver un travail après délivrance du diplôme</p>
                </div>
            </div>

            <div class="side-formation" data-aos="fade-up">
                <div class="right-side-formation large-text left-text-large">
                    <p>Des modes pédagogiques différents sont mis en place, qui conviennent ainsi à un plus grand nombre d'étudiants et de professionnels souhaitant reprendre des études ou faire une validation d'acquis de l'expérience (VAE).</p>
                </div>
                <div class="left-side-formation image-formation">
                    <h1>VAE</h1>
                </div>
            </div>

            <div class="side-formation" data-aos="fade-up">
                <div class="left-side-formation image-formation">
                    <h1><i class="fas fa-globe-americas"></i></h1>
                </div>
                <div class="right-side-formation large-text">
                    <p>les IUT, en partenariat avec des universités étrangères, proposent des poursuites d'études à l'étranger, ainsi que des stages.</p>
                </div>
            </div>

        <h1 class="carousel-header connexe-title">Les départements informatiques</h1>

        <div class="photo-side-formation">
            <div class="photo-formation">
                <canvas id="canvas-formation" class="canvas-image-formation"></canvas>
            </div>
            <div class="long-text-formation">
                <div class="text-container-formation">
                    <p>L'enseignement de l'informatique à l'IUT fait une large part à la dimension professionnelle tout en conservant des composantes fondamentales; l'étudiant acquiert ainsi des savoir-faires mais également des connaissances, des concepts de base et des méthodes de travail qu'il sera à même de mettre à profit en situation professionnelle.</p>
                    <p>Le DUT, diplôme universitaire et professionnel (niveau L2 du schéma LMD, 120 crédits européens):
                    Les titulaires du DUT en informatique sont des techniciens supérieurs, capables de participer à la conception, la réalisation et la mise en oeuvre de systèmes informatiques. Ils peuvent cependant s'ils le désirent poursuivre leurs études à l'issue de ce diplôme.</p>
                    <p>La Licence Professionnelle «Systèmes Informatiques et Logiciels» (SIL), diplôme de niveau L3 dans le schéma LMD (grade de Licence, 180 crédits européens): ces pages présentent les licences professionnelles proposées dans les départements informatiques d'IUT.</p>
                    <p>Les titulaires de la licence SIL sont des spécialistes capables de participer à un projet de développement logiciel et, à terme, d'en prendre la complète responsabilité.</p>
                </div>
    		</div>
        </div>

        <h1 class="carousel-header connexe-title">Programme officiel</h1>

        <div class="home-feed">
            <div class="formation-flex">
                <div class="axe-formation download-axe" data-aos="fade-up" data-aos-delay="200">
                    <a href="{{asset('files/fr.pdf')}}" target="_blank">
                        <div class="number-axe-formation">
                            <i class="fas fa-file-download"></i>
                        </div>
                    </a>
                    <p>Version française</p>
                </div>
                <div class="axe-formation download-axe" data-aos="fade-up" data-aos-delay="500">
                    <a href="{{asset('files/de.pdf')}}" target="_blank">
                        <div class="number-axe-formation">
                            <i class="fas fa-file-download"></i>
                        </div>
                    </a>
                    <p>Deutsche Version</p>
                </div>
                <div class="axe-formation download-axe" data-aos="fade-up" data-aos-delay="800">
                    <a href="{{asset('files/es.pdf')}}" target="_blank">
                        <div class="number-axe-formation">
                            <i class="fas fa-file-download"></i>
                        </div>
                    </a>
                    <p>Version en español</p>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/granim.min.js') }}"></script>
        <script>
            var granimInstance = new Granim({
                element: '#canvas-formation',
                direction: 'diagonal',
                isPausedWhenNotInView: true,
                image : {
                    source: './img/abstract-business-code-270348.jpg',
                    blendingMode: 'multiply',
                    position: ['center','center'],
                    stretchMode: ['stretch', 'none'],
                },
                states : {
                    "default-state": {
                        gradients: [
                                ['#69FBA8', '#35E3AE'],
                                ['#35E3AE', '#48ACD9'],
                                ['#48ACD9', '#5257E1'],
                                ['#5257E1', '#5449A1']
                        ],
                        transitionSpeed: 4000
                    }
                }
            });
        </script>

@endsection

