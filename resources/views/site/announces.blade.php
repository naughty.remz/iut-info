@extends ('layouts.site')

@section('title', 'Annonces')

@section('content')
    <section class="">
        <div class="header-connexe">
            <div class="title-accueil">
                <h1>@lang('internship.home')</h1>
            </div>
        </div>
        <div class="contentContainer">
            <p>
                <br>@lang('internship.search')
                <a style="text-decoration: underline; color: blue;" href="{{ url('nouvelle-annonce') }}">@lang('internship.click')</a>
                <br>
            </p>
            <ul class="announces-container">
                @foreach($announces as $announce)
                    <li>
                        <a href="/annonces/{{ $announce->id }}">
                            @if(  $announce->type  == 'stage')
                                <strong class="title"><i class="fas fa-book-open stage"></i> <i class="fas fa-hard-hat"></i>- {{ $announce->title }}</strong>
                            @elseif( $announce->type  == 'apprentissage' )
                                <strong class="title apprentissage"><i class="far fa-handshake"></i> - {{ $announce->title }}</strong>
                            @else
                                <strong class="title">{{ $announce->title }}</strong>
                            @endif
                        </a>

                        <p><strong>Entreprise :</strong> {{ $announce->company_name }} - {{ $announce->company_city }} ( {{ $announce->company_country }} )</p>

                        @if (!is_null($announce->contact_email) and !is_null($announce->contact_phone))
                            <p><strong>Contact :</strong> {{ $announce->contact_phone }} - {{ $announce->contact_email }}</p>
                        @elseif(!is_null($announce->contact_email) and is_null($announce->contact_phone))
                            <p><strong>Contact :</strong> {{ $announce->contact_email }}</p>
                        @elseif(is_null($announce->contact_email) and !is_null($announce->contact_phone))
                            <p><strong>Contact :</strong> {{ $announce->contact_phone }}</p>
                        @endif
                        <p><strong>Début :</strong> {{ $announce->date_beginning }}</p>
                        <p><strong>Durée minimum :</strong> {{ $announce->minimum_duration }}</p>

                        {{-- <p><strong>Description :</strong> {!! nl2br(e($announce->description)) !!}</p> --}}
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="contentContainer">
            {{ $announces->links('vendor.pagination.default-iut', ['elements' => $announces]) }}
        </div>
    </section>
@endsection

@section('javascript')
    <script>
        @if(session()->has('status'))
        Swal.fire('Annonce envoyée', '{{ session('status') }}', 'success');
        @endif
    </script>

    <script>
        $(function(){
                $("#entreprises").addClass("open");
                $("#annonces").addClass("here");
            });
    </script>
@endsection
