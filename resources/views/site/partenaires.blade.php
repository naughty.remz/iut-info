@extends ('layouts.site')

@section('title', 'Partenaires')

@section('content')

    <div class="header-connexe">
        <div class="title-accueil">
            <h1>@lang('partners.header')</h1>
        </div>
    </div>
    
    <div class="contentContainer">
        <h2 class="typicalTitle wrapper">@lang('partners.title')</h1>
        <p  class="typicalText wrapper">@lang('partners.intro')</p>
        <p class="typicalText wrapper">@lang('partners.listIntro')</p>
    </div>

    <div class="home-feed">
        <div class="partenaires-flex">
            <div class="partenaire">
                <a href="https://atos.net/fr/" target="_blank">
                    <!-- Atos -->
                    <img src="{{asset('img/1280px-Atos_logo.png')}}">
                </a>
            </div>
            <div class="partenaire test">
                <a href="https://atos.net/en/products?utm_source=bull.com/&utm_medium=301" target="_blank">
                    <!-- Bull -->
                    <img src="{{asset('img/logo-bull.png')}}">
                </a>
            </div>
            <div class="partenaire">
                <a href="https://www.capgemini.com/fr-fr/" target="_blank">
                    <!-- Capgemini -->
                    <img src="{{asset('img/cg_logo.png')}}">
                </a>
            </div>
            <div class="partenaire">
                <a href="https://www.cigref.fr/" target="_blank">
                    <!-- Cigref -->
                    <img src="{{asset('img/Logo-cigref-2016.png')}}">
                </a>
            </div>
            <div class="partenaire">
                <a href="https://www.ibm.com/fr-fr/" target="_blank">
                    <!-- IBM -->
                    <img src="{{asset('img/logo-IBM.png')}}">
                </a>
            </div>
            <div class="partenaire">
                <a href="https://www.microsoft.com/fr-fr/" target="_blank">
                    <!-- Microsoft -->
                    <img src="{{asset('img/raw.png')}}">
                </a>
            </div>
            <div class="partenaire">
                <a href="https://www.syntec.fr/" target="_blank">
                    <!-- Syntec -->
                    <img src="{{asset('img/Logo_federation_Syntec.png')}}">
                </a>
            </div>
            <div class="partenaire">
                <a href="https://www.cgi.fr/fr-fr" target="_blank">
                    <!-- Unilog -->
                    <img src="{{asset('img/Logo_Strap.tif.png')}}">
                </a>
            </div>
        </div>
    </div>

    <div class="contentContainer">
        <h2 class="typicalTitle wrapper">@lang('partners.internationalTitle')</h1>
        <p  class="typicalText wrapper">@lang('partners.internationalIntro')</p>
        <p class="typicalText wrapper">@lang('partners.internationalListIntro')</p>
    </div>
    <div class="home-feed">
        <div class="partenaires-flex">
            <div class="partenaire">
                <a href="http://www.unb.br/" target="_blank">
                    <!-- Atos -->
                    <img src="{{asset('img/universidade-de-brasilia-logo.png')}}">
                </a>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(function(){
                $("#entreprises").addClass("open");
                $("#partenaires").addClass("here");
            });
    </script>
@endsection