@extends ('layouts.site')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{asset('css/odometer-theme-default.css')}}">

    <div class="header-connexe">
        <div class="title-accueil">
            <h1>@lang('presentation.header')</h1>
        </div>
    </div>
    
    <h1 class="carousel-header connexe-title">@lang('presentation.title')</h1>

    <div class="home-feed">

        <div class="content-stats">
            <div class="special-stats">
                <div class="content-odometer"><h1 class="odometer odometer1"></h1></div>
                <div class="content-stats">
                    <p>@lang('presentation.nbIut')</p>
                </div>
            </div>
            <div class="special-stats">
                <div class="content-odometer"><h1 class="odometer odometer2"></h1></div>
                <div class="content-stats">
                    <p>@lang('presentation.nbAns')</p>
                </div>
            </div>
        </div>

        <div class="formation-subtitle">
            <h3>@lang('presentation.3forces')</h3>
        </div>

        <div class="formation-flex">
            <div class="axe-formation" data-aos="fade-up" data-aos-delay="200">
                <div class="number-axe-formation">
                    <h1>@lang('presentation.1')</h1>
                </div>
                <p>@lang('presentation.1detail')</p>
            </div>
            <div class="axe-formation" data-aos="fade-up" data-aos-delay="500">
                <div class="number-axe-formation">
                    <h1>@lang('presentation.2')</h1>
                </div>
                <p>@lang('presentation.2detail')</p>
            </div>
            <div class="axe-formation" data-aos="fade-up" data-aos-delay="800">
                <div class="number-axe-formation">
                    <h1>@lang('presentation.3')</h1>
                </div>
                <p>@lang('presentation.3detail')</p>
            </div>
        </div>
    </div>
        <div class="side-formation" data-aos="fade-up">
            <div class="left-side-formation image-formation">
                <h1>@lang('presentation.6')</h1>
                <h3>@lang('presentation.mois')</h3>
            </div>
            <div class="right-side-formation large-text">
                <p>@lang('presentation.tpMoy')</p>
            </div>
        </div>

        <div class="side-formation" data-aos="fade-up">
            <div class="right-side-formation large-text left-text-large">
                <p>@lang('presentation.VAEdetails')</p>
            </div>
            <div class="left-side-formation image-formation">
                <h1>@lang('presentation.VAE')</h1>
            </div>
        </div>

        <div class="side-formation" data-aos="fade-up">
            <div class="left-side-formation image-formation">
                <h1><i class="fas fa-globe-americas"></i></h1>
            </div>
            <div class="right-side-formation large-text">
                <p>@lang('presentation.etranger')</p>
            </div>
        </div>

    <h1 class="carousel-header connexe-title">@lang('presentation.depInfo')</h1>

    <div class="photo-side-formation">
        <div class="photo-formation">
            <canvas id="canvas-formation" class="canvas-image-formation"></canvas>
        </div>
        <div class="long-text-formation">
            <div class="text-container-formation">
                <p>@lang('presentation.depInfoDetails1')</p>
                <p>@lang('presentation.depInfoDetails2')</p>
                <p>@lang('presentation.depInfoDetails3')</p>
                <p>@lang('presentation.depInfoDetails4')</p>
            </div>
        </div>
    </div>

    
    <script src="{{ asset('js/granim.min.js') }}"></script>
    <script>
        var granimInstance = new Granim({
            element: '#canvas-formation',
            direction: 'diagonal',
            isPausedWhenNotInView: true,
            image : {
                source: './img/abstract-business-code-270348.jpg',
                blendingMode: 'multiply',
                position: ['center','center'],
                stretchMode: ['stretch', 'none'],
            },
            states : {
                "default-state": {
                    gradients: [
                            ['#69FBA8', '#35E3AE'],
                            ['#35E3AE', '#48ACD9'],
                            ['#48ACD9', '#5257E1'],
                            ['#5257E1', '#5449A1']
                    ],
                    transitionSpeed: 4000
                }
            }
        });
    </script>

@endsection

