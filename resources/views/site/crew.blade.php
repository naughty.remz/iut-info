<!DOCTYPE html>
<html>
<head>
	<title>Made By Crew Core</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="{{asset('css/fonts.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/crew.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/aos.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bounce.css') }}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
</head>
<body class="crewcore">
	<div class="title-crew" data-aos="fade-in" id="crewcore">
		<h3 class="subtitle-crew">Site web propulsé par le</h3>
		<h1 class="crew-title">Crew Core</h1>
	</div>
	<div class="members-crew">
		<div class="crew" data-aos="fade-up" data-aos-delay="300">
			<div class="crew-avatar">
				<img src="{{ asset('img/2xHW7h3FQk2N2tgn31qgpg_thumb_165d.jpg') }}">
			</div>
			<div class="crew-name">
				<h2>Contarin <br/>Julien</h2>
			</div>
			<div class="crew-desc">
				<p>Développeur back-end</p>
			</div>
		</div>
		<div class="crew" data-aos="fade-up" data-aos-delay="400">
			<div class="crew-avatar">
				<img src="{{ asset('img/51221253_623522908087053_3037900737204977664_n.jpg') }}">
			</div>
			<div class="crew-name">
				<h2>Despiau-Pujo Valentin</h2>
			</div>
			<div class="crew-desc">
				<p>Développeur back-end</p>
			</div>
		</div>
		<div class="crew" data-aos="fade-up" data-aos-delay="500">
			<div class="crew-avatar">
				<img src="{{ asset('img/51573016_2170761129629507_9079427122757369856_n.jpg') }}">
			</div>
			<div class="crew-name">
				<h2>Lamothe <br/> Thomas</h2>
			</div>
			<div class="crew-desc">
				<p>Développeur front-end</p>
			</div>
		</div>
		<div class="crew" data-aos="fade-up" data-aos-delay="600">
			<div class="crew-avatar">
				<img src="{{ asset('img/remz.jpg') }}">
			</div>
			<div class="crew-name">
				<h2>Ménor <br /> Rémi</h2>
			</div>
			<div class="crew-desc">
				<p>Développeur Full Stack</p>
				<p>Chef de Projet</p>
			</div>
		</div>
		<div class="crew" id="ptg" style="display: none;">
			<div class="crew-avatar">
				<img src="{{ url('https://scontent-mrs1-1.xx.fbcdn.net/v/t1.0-9/22688627_142958859783862_9113253324454892296_n.jpg?_nc_cat=108&_nc_ht=scontent-mrs1-1.xx&oh=82aad09cfc1f1e408b992f9491dc5b45&oe=5CB75E6F') }}">
			</div>
			<div class="crew-name">
				<h2>Petit<br />Guillaume</h2>
			</div>
			<div class="crew-desc">
				<p>Membre du Crew Core</p>
			</div>
		</div>
	</div>
	<div class="button-retour-accueil">
		<a href="/">Retourner à l'accueil</a>
	</div>

	<script>
		var k = [80, 84, 71], 
		n = 0;
		$(document).keydown(function (e) {
			if (e.keyCode === k[n++]) {
				if (n === k.length) {
					$('#ptg').css('display', 'inline-block');
					n = 0;
					return false;
				}
			}
			else {
				n = 0;
			}
		});


		$('#crewcore').click(function(){
			Swal.fire({
				type: 'success',
				title: 'Cadeau du Crew',
				text: "Le Crew Core t'offre un easter-egg ! Tape le code 'bounce' sur n'importe quelle page du site pour activer un mode secret !"
			});
		});
		
	</script>
	<script>
	// EASTER EGG
	$(function () {
		var k = [66, 79, 85, 78, 67, 69];
		var n = 0;
		var bounce = false;
		$(document).keydown(function (e) {
			if (e.keyCode === k[n++]) {
				if (n === k.length) {
					Swal.fire({
						type: 'success',
						title: 'Bounce mode',
						text: 'Click on any text to make it bounce !'
					});
					bounce = true;

					n = 0;
					return false;
				}
			}
			else {
				n = 0;
			}
		});

		$('p, h1, h2, h3, h4, h5, h6, img').click(function () {
			if (!$(this).hasClass("animation-target") & bounce == true ? $(this).addClass("animation-target") : $(this).removeClass("animation-target"));
		});
	});
	</script>

	<script src="{{ asset('js/aos.js') }}"></script>

    <script type="text/javascript">
    	$(document).ready(function(){
    		AOS.init();
    	});
    </script>
</body>
</html>