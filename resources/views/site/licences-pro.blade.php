@extends ('layouts.site')

@section('title', 'Licences Professionnelles')

@section('content')
<div class="header-connexe">
    <div class="title-accueil">
        <h1>@lang('LP.header')</h1>
    </div>
</div>
<div class="contentContainer">
    <div class="contentSubcontainer">
        <h1 class="typicalTitle wrapper">@lang('LP.intro')</h1>
        <p class="typicalText wrapper">@lang('LP.introText')</p>
        <ul class="typicalText wrapperList">
            <li>- @lang('LP.force1')</li>
            <li>- @lang('LP.force3')</li>
            <li>- @lang('LP.force4')</li>
            <li>- @lang('LP.force2')</li>
            <li>- @lang('LP.force5')</li>
            <li>- @lang('LP.force6')</li>
        </ul>
    </div>
    <div class="contentSubcontainer">
        <h2 class="typicalSubtitle wrapper">@lang('LP.LPSpecialHeader')</h2>
        <p class="typicalText wrapper">@lang('LP.LPSpecialText')</p>
    </div>
    <div class="contentSubcontainer">
        <h2 class="typicalSubtitle wrapper">@lang('LP.LPDoubleCompHeader')</h2>
        <p class="typicalText wrapper">@lang('LP.LPDoubleCompText')</p>

        <div data-aos="fade-left" data-aos-delay="150" class="typical-button-holder">
            <a href="/licences-pro-specialisation">@lang('LP.LPDiscover')</a>
        </div>
    </div>
    

    
</div>

@endsection

@section('javascript')
    <script>
        $(function(){
                $("#formations").addClass("open");
                $("#lp").addClass("here");
            });
    </script>
@endsection