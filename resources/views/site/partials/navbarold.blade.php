<div class="leftNavigation">
    <div class="logo">
        <a href="/"><img src="{{ asset('img/logoW.png') }}"></a>
    </div>
    <nav id="principal-navbar">
        <ul>
            <li>
                <a href="/" class="item">
                    <i class="fa fa-home"></i>
                    <p> @lang('navbar.home')</p>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-graduation-cap"></i>
                    <p>Les formations</p>
                </a>
                <ul class="submenu">
                    <li>
                        <a href="/dut-info">Le DUT Informatique</a>
                    </li>
                    <li>
                        <a href="/licences-pro">Les Licences professionnelles</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="far fa-lightbulb"></i>
                    <p> @lang('navbar.about')</p>
                </a>
                <ul class="submenu">
                    <li><a href="/presentation">@lang('navbar.pres')</a></li>
                    <li><a href="/formation">@lang('navbar.formation')</a></li>
                    <li><a href="/iut-france">@lang('navbar.IUTInFr')</a></li>
                </ul>
            </li>
            <li>
                <a href="/actualites">
                    <i class="far fa-newspaper"></i>
                    <p>Actualités</p>
                </a>
            </li>
            <li>
                <a href="/temoignages">
                    <i class="fas fa-comment-alt"></i>
                    <p>  @lang('navbar.tem') </p>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="far fa-handshake"></i>
                    <p>  @lang('navbar.companies')</p>
                </a>
                <ul class="submenu">
                    <li><a href="/partenaires">@lang('navbar.part')</a></li>
                    <li><a href="/annonces">@lang('navbar.offers')</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-graduation-cap"></i>
                    <p>  @lang('navbar.after')</p>
                </a>
                <ul class="submenu">
                    <li><a href="/debouches">@lang('navbar.deb')</a></li>
                    <li><a href="/poursuite">@lang('navbar.studies')</a></li>
                </ul>
            </li>
        </ul>
    </nav>

</div>
