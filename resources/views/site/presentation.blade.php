@extends ('layouts.site')

@section('title', 'Presentation')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{asset('css/odometer-theme-default.css')}}">

    <div class="header-connexe">
        <div class="title-accueil">
            <h1>@lang('presentation.header')</h1>
        </div>
    </div>

    
    <div class="contentContainer">
        <div class="contentSubcontainer">
            <h2 class="typicalTitle wrapper">@lang('presentation.title')</h2>
            <div class="statsContainer wrapper">
                <div>
                    <p class="number odometer odometer1"></p>

                    <p class="subtext">@lang('presentation.nbIut')</p>
                </div>
                <div>
                    <p class="number odometer odometer2"></p>
                    <p class="subtext">@lang('presentation.nbAns')</p>
                </div>
            </div>
        </div>

        <div class="contentSubcontainer">
            <h2 class="typicalSubtitle wrapper">@lang('presentation.3forces')</h2>
            <div class="bubbleContainer wrapper">
                <div class="bubble">
                    <div class="round" data-aos="fade-zoom-in" data-aos-delay="300">
                        <p>@lang('presentation.1')</p>
                    </div>
                    <p data-aos="fade-up" data-aos-delay="600" class="typicalText">@lang('presentation.1detail')</p>
                </div>
                <div class="bubble">
                    <div class="round" data-aos="fade-zoom-in" data-aos-delay="300">
                        <p>@lang('presentation.2')</p>
                    </div>
                    <p data-aos="fade-up" data-aos-delay="800" class="typicalText">@lang('presentation.2detail')</p>
                </div>
                <div class="bubble">
                    <div class="round" data-aos="fade-zoom-in" data-aos-delay="300">
                        <p>@lang('presentation.3')</p>
                    </div>
                    <p data-aos="fade-up" data-aos-delay="1000" class="typicalText">@lang('presentation.3detail')</p>
                </div>

            </div>
        </div>

        <div class="contentSubcontainer">
            <div class="horizontalDisplay wrapper">
                <div data-aos="fade-right" data-aos-delay="300">
                    <p class="big">@lang('presentation.6')</p>
                    <p class="sub">@lang('presentation.mois')</p>
                </div>
                <p class="typicalText wrapper" data-aos="fade-left" data-aos-delay="300">@lang('presentation.tpMoy')</p>
            </div>
            <div class="horizontalDisplay wrapper">
                <p class="typicalText wrapper" data-aos="fade-right" data-aos-delay="300">@lang('presentation.VAEdetails')</p>
                <div data-aos="fade-left" data-aos-delay="300">
                    <p class="big">@lang('presentation.VAE')</p>
                </div>
            </div>
            <div class="horizontalDisplay wrapper">
                <div data-aos="fade-right" data-aos-delay="300">
                    <p class="big"><i class="fas fa-globe-americas"></i></p>
                </div>
                <p class="typicalText wrapper" data-aos="fade-left" data-aos-delay="300">@lang('presentation.etranger')</p>
            </div>
            
        </div>

        <div class="contentSubcontainer">
            <h2 class="typicalSubtitle wrapper">@lang('presentation.depInfo')</h2>
            <p class="typicalText wrapper">@lang('presentation.depInfoDetails1')</p>
            <p class="typicalText wrapper">@lang('presentation.depInfoDetails2')</p>
            <p class="typicalText wrapper">@lang('presentation.depInfoDetails3')</p>
            <p class="typicalText wrapper">@lang('presentation.depInfoDetails4')</p>
        </div>
    </div>

{{--     
    <script src="{{ asset('js/granim.min.js') }}"></script>
    <script>
        var granimInstance = new Granim({
            element: '#canvas-formation',
            direction: 'diagonal',
            isPausedWhenNotInView: true,
            image : {
                source: './img/abstract-business-code-270348.jpg',
                blendingMode: 'multiply',
                position: ['center','center'],
                stretchMode: ['stretch', 'none'],
            },
            states : {
                "default-state": {
                    gradients: [
                            ['#69FBA8', '#35E3AE'],
                            ['#35E3AE', '#48ACD9'],
                            ['#48ACD9', '#5257E1'],
                            ['#5257E1', '#5449A1']
                    ],
                    transitionSpeed: 4000
                }
            }
        });
    </script> --}}

@endsection

@section('javascript')
    <script src="{{ mix('dist/js/formation.js')}}"></script>
    <script>
        $(function(){
                $("#iut").addClass("open");
				$("#presentation").addClass("here");
            });
    </script>
@endsection 


