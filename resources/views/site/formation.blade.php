@extends ('layouts.site')

@section('title', 'Formation')

@section('content')


    <div class="header-connexe">
        <div class="title-accueil">
            <h1>@lang('formation.header')</h1>
        </div>
    </div>
    <div class="contentContainer">

        <div class="contentSubcontainer">
            <h2 class="typicalTitle wrapper">@lang('formation.intro')</h2>
            <div class="imageText wrapper">
                <p class="typicalText wrapper">@lang('formation.sentence1')</p>
                <img data-aos="fade-zoom-in" data-aos-delay="200" src="{{ asset('img/icons/undraw_programmer_imem.svg') }}" alt=""/>
            </div>
        </div>

        


    </div>
    
    <div class="background-grey">
        <div class="container-axe">
            <div class="axe" data-aos="fade-up" data-aos-delay="300">
                <div class="icon element-blue">
                    <img src="{{ asset('img/idea-blue.svg') }}">
                </div>
                <div class="title-axe element-blue">
                    <span class="side-tiret"></span><p>@lang('formation.analyseTitle')</p><span class="side-tiret"></span>
                </div>
                <div class="text-container-axe">
                    <p>@lang('formation.analyseDesc')</p>
                </div>
            </div>
            <div class="axe" data-aos="fade-up" data-aos-delay="500">
                <div class="icon element-red">
                    <img src="{{ asset('img/web-programming.svg') }}">
                </div>
                <div class="title-axe element-red">                        
                    <span class="side-tiret"></span><p>@lang('formation.algoTitle')</p><span class="side-tiret"></span>
                </div>
                <div class="text-container-axe">
                    <p>@lang('formation.algoDesc')</p>
                </div>
            </div>
            <div class="axe" data-aos="fade-up" data-aos-delay="700">
                <div class="icon element-yellow">
                    <img src="{{ asset('img/gears.svg') }}">
                </div>
                <div class="title-axe element-yellow">
                    <span class="side-tiret"></span><p>@lang('formation.archiTitle')</p><span class="side-tiret"></span>
                </div>
                <div class="text-container-axe">
                    <p>@lang('formation.archiDesc')</p>
                </div>
            </div>
        </div>
    </div>


    <div class="hero-container">
        <div class="hero-text-holder">
            <p class="hero-text hero-presentation">@lang('formation.ajouts')</p>
            <div class="hero-subtext">
                <div class="vignette" data-aos="fade-up" data-aos-delay="300">
                    <img src="{{ asset('img/sum.png') }}">
                    <p>@lang('formation.math')</p>
                </div>
                <div class="vignette" data-aos="fade-up" data-aos-delay="400">
                    <img src="{{ asset('img/euro.png') }}">
                    <p>@lang('formation.eco')</p>
                </div>
                <div class="vignette" data-aos="fade-up" data-aos-delay="500">
                    <img src="{{ asset('img/notebook.png') }}">
                    <p>@lang('formation.gest')</p>
                </div>
                <div class="vignette" data-aos="fade-up" data-aos-delay="600">
                    <img src="{{ asset('img/english-language.png') }}">
                    <p>@lang('formation.ang')</p>
                </div>
                <div class="vignette" data-aos="fade-up" data-aos-delay="700">
                    <img src="{{ asset('img/speech-bubble.png') }}">
                    <p>@lang('formation.com')</p>
                </div>
            </div>

        </div>
        <canvas id="canvas-presentation" class="canvas-image-blending"></canvas>
    </div>  




    <div class="contentSubcontainer">
        <div class="imageText wrapper">
            <img data-aos="fade-zoom-in" data-aos-delay="200" src="{{ asset('img/icons/undraw_coding_6mjf.svg') }}" alt=""/>
            <p class="typicalText wrapper">@lang('formation.lastText')</p>
        </div>
    </div>

    <div class="contentSubcontainer">
        <h2 class="typicalSubtitle wrapper">@lang('formation.prog')</h2>
        <div class="bubbleContainer wrapper">
            
            <div class="bubble">
                    <a href="/files/fr.pdf">
                        <div class="file" data-aos="fade-zoom-in" data-aos-delay="200">
                                <i class="fas fa-file-download"></i>
                        </div>
                    </a>
                <p data-aos="fade-up" data-aos-delay="400" class="typicalText">@lang('formation.fr')</p>
            </div>

            <div class="bubble">
                    <a href="/files/de.pdf">
                        <div class="file" data-aos="fade-zoom-in" data-aos-delay="200">
                                <i class="fas fa-file-download"></i>
                        </div>
                    </a>
                <p data-aos="fade-up" data-aos-delay="400" class="typicalText">@lang('formation.de')</p>
            </div>

            <div class="bubble">
                    <a href="/files/es.pdf">
                        <div class="file" data-aos="fade-zoom-in" data-aos-delay="200">
                                <i class="fas fa-file-download"></i>
                        </div>
                    </a>
                <p data-aos="fade-up" data-aos-delay="400" class="typicalText">@lang('formation.es')</p>
            </div>
            



        </div>
    </div>

























    <script src="{{ asset('js/granim.min.js') }}"></script>
    <script>
        var granimInstance = new Granim({
            element: '#canvas-presentation',
            direction: 'diagonal',
            isPausedWhenNotInView: true,
            image : {
                source: './img/rawpixel-678089-unsplash.jpg',
                blendingMode: 'multiply',
                position: ['center','center'],
                stretchMode: ['stretch', 'none'],
            },
            states : {
                "default-state": {
                    gradients: [
                            ['#69FBA8', '#35E3AE'],
                            ['#35E3AE', '#48ACD9'],
                            ['#48ACD9', '#5257E1'],
                            ['#5257E1', '#5449A1']
                    ],
                    transitionSpeed: 4000
                }
            }
        });
    </script>

@endsection

@section('javascript')
    <script>
        $(function(){
                $("#iut").addClass("open");
				$("#formation").addClass("here");
            });
    </script>
@endsection 
