@extends ('layouts.site')

@section('title', 'Témoignages')

@section('head')
    <link rel="stylesheet" href="./css/carousel.css">
@endsection

@section('content')
    <div class="header-connexe">
        <div class="title-accueil"><h1>@lang('testimonial.home')</h1></div>
    </div>

    {{-- <div class="cardContainer" style="min-height: 320px;">
        <strong>Page en travaux ! </strong>
    </div> --}}

    <div class="temoignageCardContainer" style="min-height: 40vh;">
        @foreach ($temoignages as $temoignage)
        <div class="temoignage-element-fix">
            <div class="testimonial-content-fix">
                <a href="/temoignages/{{ $temoignage->id}}">
                    @if( !is_null($temoignage->image) )
                        <div style="background-image: url(storage/{{$temoignage->image }});"
                                class="testimonial-img-fix"></div>
                    @else
                        <div style="background-image: url({{ asset('img/user_placeholder.png') }});"
                                class="testimonial-img-fix"></div>
                    @endif
                    <p class="testimonial-name-fix">{{ $temoignage->author }}</p>
                    <p class="testimonial-job-fix">{{ $temoignage->author_info }}</p>
                    <div class="quote-holder-fix">
                        <img src="{{ asset('img/quote.png')}}" alt="" class="quote-fix">
                        <img src="{{ asset('img/quote.png')}}" alt="" class="quote-bottom">
                    </div>
                    <p class="testimonial-text">{{ str_limit($temoignage->description, $limit = 30, $end = '...') }}</p>
                    <p class="testimonial-description" style="height: 100px; opacity: 1;">
                        {{ str_limit($temoignage->description, $limit = 150, $end = '...') }}
                    </p>
                </a>

            </div>
        </div>

        @endforeach
    </div>
    <div class="contentContainer">
        {{ $temoignages->links('vendor.pagination.default-iut', ['elements' => $temoignages]) }}
    </div>

@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
            $('.testimonial-description').hide();
        });

        $(document).ready(function () {
            $('.testimonial-content-fix').hover(function () {
                $(this).find('.testimonial-description').stop().animate({
                    height: "toggle",
                    opacity: "toggle",
                    'padding-bottom': "toggle"
                }, 270);
                $(this).find('.testimonial-text').stop().animate({
                    height: "toggle",
                    opacity: "toggle"
                }, 270);
            });
        });
    </script>

    <script>
        $(function(){
                $("#temoignages").addClass("open");
            });
    </script>
@endsection
