@extends ('layouts.site')

@section('content')
    <div class="header-connexe">
        <div class="title-accueil"><h1>{{ $actu->title }}</h1></div>
    </div>
    <div class="contentContainer">
        @if ( !is_null($actu->author))
            <p style="margin-bottom: 20px;"><strong>{{ $actu->author }}</strong>, le <span class="number">{{ \Carbon\Carbon::parse($actu->created_at)->format('d/m/y') }}</span></p>
        @else
            <p style="margin-bottom: 20px;" class="number">{{ \Carbon\Carbon::parse($actu->created_at)->format('d/m/y') }}</p>
        @endif

        @if (!is_null($actu->photo))
            <img class="img-article" src="../storage/{{ $actu->photo }}" alt="Illustration article {{ $actu->title }}">
        @endif
        <p class="typicalText wrapper">
            {!! nl2br(e( $actu->description )) !!}
            {{-- <br><br> --}}
        </p>
    </div>
@endsection

