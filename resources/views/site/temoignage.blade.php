@extends ('layouts.site')

@section('title', 'Témoignage')

@section('content')
    <div class="header-connexe">
        <div class="title-accueil"><h1>{{ $temoignage->author}}</h1></div>
    </div>


    <div class="contentContainer">
        <div class="contentSubcontainer">
            @if( !is_null($temoignage->image) )
                <div style="background-image: url(../storage/{{$temoignage->image }});" class="profil-pic"></div>
            @else
                <div style="background-image: url({{ asset('img/user_placeholder.png') }});" class="profil-pic"></div>
            @endif
            <p class="author-info wrapper">{{$temoignage->author_info }}</p>
            <p class="typicalSubtitle wrapper">{{$temoignage->title }}</p>
            <p class="typicalText wrapper">{!! nl2br(e( $temoignage->description )) !!}</p>
        </div>

    </div>


@endsection
