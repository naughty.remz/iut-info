@extends ('layouts.site')

@section('title', 'Proposer une annonce')


@section('content')
    <section class="new-announce">
        <div class="header-connexe">
            <div class="title-accueil">
                <h1>Poster une annonce de stage / apprentissage</h1>
            </div>
        </div>

        <div class="contentContainer">
            <!-- <a class="button-AddAnnounce" href="annonces">Retour à la liste des annonces</a> -->

            <form method="POST" action="{{ url("/nouvelle-annonce") }}">
                {{ csrf_field() }}
                <div class="fields">
                    <div>
                        <h2 class="subtitle">Entreprise</h2>
                        <div class="field">
                            <label class="label" for="company_name">Raison sociale</label>
                            <input class="input" id="company_name" name="company_name" type="text" value="{{ old('company_name') }}">
                        </div>
                        <div class="field">
                            <label class="label" for="company_country">Pays</label>
                            <input class="input" id="company_country" name="company_country" type="text" value="{{ old('company_country') }}">
                        </div>
                        <div class="field">
                            <label class="label" for="company_zipcode">Code postal</label>
                            <input class="input" id="company_zipcode" name="company_zipcode" type="text" value="{{ old('company_zipcode') }}">
                        </div>
                        <div class="field">
                            <label class="label" for="company_city">Ville</label>
                            <input class="input" id="company_city" name="company_city" type="text" value="{{ old('company_city') }}">
                        </div>
                        <div class="field">
                            <label class="label" for="company_address">Adresse</label>
                            <input class="input" id="company_address" name="company_address" type="text" value="{{ old('company_address') }}">
                        </div>
                    </div>
                    <div>
                        <h2 class="subtitle">Contact dans l'entreprise</h2>
                        <div class="field">
                            <label class="label" for="contact_lastname">Nom</label>
                            <input class="input" id="contact_lastname" name="contact_lastname" type="text" value="{{ old('contact_lastname') }}">
                        </div>
                        <div class="field">
                            <label class="label" for="contact_firstname">Prénom</label>
                            <input class="input" id="contact_firstname" name="contact_firstname" type="text" value="{{ old('contact_firstname') }}">
                        </div>
                        <div class="field">
                            <label class="label" for="contact_phone">N° téléphone</label>
                            <input class="input" id="contact_phone" name="contact_phone" type="text" value="{{ old('contact_phone') }}">
                        </div>
                        <div class="field">
                            <label class="label" for="contact_email">Email</label>
                            <input class="input" id="contact_email" name="contact_email" type="email" value="{{ old('contact_email') }}">
                        </div>
                    </div>
                    <div>
                        <h2 class="subtitle">Infos générales</h2>
                        <div class="field">
                            <label class="label" for="title">Intitulé du stage</label>
                            <input class="input" id="title" name="title" type="text" value="{{ old('title') }}">
                        </div>
                        <div class="field">
                            <label class="label">Type d'annonce</label>
                            <div class="control">
                                <label class="radio" for="type-announce-stage">
                                    <input value="stage" type="radio" id="type-announce-stage" name="type" required>
                                    Stage
                                </label>
                                <label class="radio" for="type-announce-apprentissage">
                                    <input value="apprentissage" type="radio" id="type-announce-apprentissage" name="type" required>
                                    Apprentissage
                                </label>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label" for="date_beginning">Date de début</label>
                            <input class="input" id="date_beginning" name="date_beginning" type="date" value="{{ old('date_beginning') }}">
                        </div>
                        <div class="field">
                            <label class="label" for="minimum_duration">Durée minimum</label>
                            <input class="input" id="minimum_duration" name="minimum_duration" type="text" value="{{ old('minimum_duration') }}">
                        </div>
                        <div class="field">
                            <label class="label" for="description">Description</label>
                            <textarea class="textarea" name="description" id="description"></textarea>
                        </div>
                    </div>
                </div>

                <div class="buttonAnnounceContainer">
                    <button class="button-AddAnnounce" type="submit">Soumettre l'annonce</button>
                </div>
            </form>
            @if($errors->any())
                <div class="contentSubcontainer">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li class="error-list" style="color: #da1818; line-height: 25px;"><i class="fas fa-exclamation-triangle"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </section>
@endsection
