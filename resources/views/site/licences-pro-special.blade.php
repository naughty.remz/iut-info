@extends ('layouts.site')

@section('title', 'Liste des Licences Professionnelles')

@section('content')
    <div class="header-connexe">
        <div class="title-accueil">
            <h1>@lang('LP.header')</h1>
        </div>
    </div>
    <div class="contentContainer">
        {{--
        <div class="filters">
            <input type="search" placeholder="Recherche par mot clé">
            <select name="" id="">
                <option value="" selected disabled>Choisissez une catégorie</option>
                <option value="">Info</option>
                <option value="">Rma</option>
                <option value="">Tique</option>
            </select>
        </div>
        --}}

        <div class="contentSubcontainer">
            {{-- <h2 class="typicalSubtitle wrapper">@lang('LP.GLSI')</h2> --}}
            @foreach ($licencesPro as $licencepro)
                @if ($licencepro->link == null)
                    <div class="lp-container wrapper">
                        <div class="head">
                            <p class="ville">{{ $licencepro->city }}</p>
                            <p class="titre">{{ $licencepro->title }}</p>
                        </div>
                        <p class="details">{{ $licencepro->details }}</p>
                        <p class="description">{{ $licencepro->description }}</p>
                    </div>
                @else
                    <div class="lp-container wrapper">
                        <a href="{{ $licencepro->link }}" target="blank">
                            <div class="head">
                                <p class="ville">{{ $licencepro->city }}</p>
                                <p class="titre">{{ $licencepro->title }}</p>
                            </div>
                        </a>
                        <p class="details">{{ $licencepro->details }}</p>
                        <p class="description">{{ $licencepro->description }}</p>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="contentContiner">
            {{ $licencesPro->links('vendor.pagination.default-iut', ['elements' => $licencesPro]) }}
        </div>
    </div>

@endsection
