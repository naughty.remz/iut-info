		$(".container").mapael({
	        map : {
	            name : "france_departments",
	            zoom : {
	            	enabled: true
	            },
	            defaultArea: {
	            	attrs :{
                        fill: "#f2f2f2",
                        stroke: "#f2f2f2"
	            	},
					attrsHover: {
                        fill: "#f2f2f2"
                    }	            
	            }
	        },
	        plots : {
                'Toulouse': {
                    latitude: 43.600000,
                    longitude: 1.433333,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Toulouse"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "https://iut.ups-tlse.fr",
                },
                'Lannion': {
                    latitude: 48.732084,
                    longitude: -3.459144,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Lannion"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut-lannion.fr/formations/INFO/index.php",               	
                },
                'vannes': {
                    latitude: 47.658236,
                    longitude: -2.760847,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Vannes"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iu-vannes.fr/Formations/INFO/info.asp",               	
                },
                'Nantes': {
                    latitude: 47.218371,
                    longitude: -1.553621,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Nantes"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iutnantes.univ-nantes.fr/",               	
                },
                'rochelle': {
                    latitude: 46.160329,
                    longitude: -1.151139,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "La Rochelle"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut-larochelle.fr/",               	
                },
                'bordeaux': {
                    latitude: 44.837789,
                    longitude: -0.57918,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Bordeaux"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut.u-bordeaux.fr/info/",               	
                },
                'limoges': {
                    latitude: 45.833619,
                    longitude: 1.261105,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Limoges"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut.unilim.fr/",               	
                },
                'blagnac': {
                    type: "image",
                    url: "/img/marker.png",
                    width: 12,
                    height: 40,
                    latitude: 43.85087,
                    longitude: 1.39703,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Blagnac"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.3",
                        fill: "#7550d7"
                    },
                    href: "http://www.iut-blagnac.fr/spip.php?mot1",               	
                },
                'laval': {
                    latitude: 48.078515,
                    longitude: -0.766991,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Laval"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut-laval.univ-lemans.fr/fr/info-informatique-et-programmation/dut-informatique.html",               	
                },
                'caen': {
                    latitude: 49.182863,
                    longitude: -0.370679,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Caen"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iutc3.unicaen.fr/info-new",               	
                },
                'havre': {
                    latitude: 49.49437,
                    longitude: 0.107929,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Le Havre"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://di.iut.univ-lehavre.fr/",               	
                },
                'calais': {
                    latitude: 50.95129,
                    longitude: 1.858686,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Calais"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://iutcalais.univ-littoral.fr/~info/",               	
                },
                'bayonne': {
                    latitude: 43.492949,
                    longitude: -1.474841,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Bayonne"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iutbayonne.univ-pau.fr/dut/info/objectifs.html",               	
                },
                'lens': {
                    latitude: 50.42893,
                    longitude: 2.83183,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Lens"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut-lens.univ-artois.fr/index2.php",               	
                },  
                'amiens': {
                    latitude: 49.894067,
                    longitude: 2.295753,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Amiens"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.info.iut-amiens.fr/",               	
                },  
                'paris': {
                    latitude: 48.856614,
                    longitude: 2.352222,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Ile De France"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut-informatique.fr/carte.php?PHPSESSID=87ad3fa9706f0137766f4e8ee558bdce#smenu5",               	
                },  
                'orleans': {
                    latitude: 47.902964,
                    longitude: 1.909251,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Orléans"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.univ-orleans.fr/iut-orleans/informatique/?page=m2_formation.htm",               	
                },  
                'clermont': {
                    latitude: 45.777222,
                    longitude: 3.087025,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Clermont-Ferrand"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://iutweb.u-clermont1.fr/index.php?option=com_content&%20task=section&id=7&Itemid=36",               	
                },  
                'puy': {
                    latitude: 45.042768,
                    longitude: 3.882936,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Le Puy"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://iutweb-lepuy.u-clermont1.fr/",               	
                },  
                'rodez': {
                    latitude: 44.349389,
                    longitude: 2.575986,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Rodez"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut-rodez.fr/",               	
                },  
                'montpellier': {
                    latitude: 43.610769,
                    longitude: 3.876716,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Montpellier"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://iutdepinfo.iutmontp.univ-montp2.fr/index.php/formations/dut-informatique",               	
                },
                'lille': {
                    latitude: 50.62925,
                    longitude: 3.057256,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Lille"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www-iut.univ-lille1.fr/info/",               	
                },  
                'maubeuge': {
                    latitude: 50.280228,
                    longitude: 3.9674,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Maubeuge"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://webmaub.univ-valenciennes.fr/info/index.php?page_p=accueil&id_sm=0",               	
                },  
                'reims': {
                    latitude: 49.258329,
                    longitude: 4.031696,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Reims"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://iut-info.univ-reims.fr/",               	
                },  
                'metz': {
                    latitude: 49.119309,
                    longitude: 6.175716,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Metz"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut.univ-metz.fr/rubrique.php3?id_rubrique=3",               	
                },  
                'die': {
                    latitude: 48.287468,
                    longitude: 6.947767,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "St Dié"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iutsd.uhp-nancy.fr/info/index2.php?action=home",               	
                },  
                'strasbourg': {
                    latitude: 48.573405,
                    longitude: 7.752111,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Strasbourg"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www-iut-schuman.u-strasbg.fr/index2.php?a=inaccueil",               	
                },  
                'belfort': {
                    latitude: 47.639674,
                    longitude: 6.863849,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Belfort"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut-bm.univ-fcomte.fr/public_website/iut/d-u-t/informatique",               	
                },  
                'nancy': {
                    latitude: 48.692054,
                    longitude: 6.184417,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Nancy"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://iut-charlemagne.univ-nancy2.fr/?contentId=5492",               	
                },  
                'dijon': {
                    latitude: 47.322047,
                    longitude: 5.04148,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Dijon"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://iutdijon.u-bourgogne.fr/internet/www",               	
                },  
                'bourg': {
                    latitude: 46.205167,
                    longitude: 5.225501,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Bourg en Bresse"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://iuta.univ-lyon1.fr/informatique_bourg.htm",               	
                },  
                'annecy': {
                    latitude: 45.899247,
                    longitude: 6.129384,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Annecy"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut-acy.univ-savoie.fr/formation/bac+2/dut/info-informatique/",               	
                },  
                'grenoble': {
                    latitude: 45.188529,
                    longitude: 5.724524,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Grenoble"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut2.upmf-grenoble.fr/",               	
                },  
                'valence': {
                    latitude: 44.933393,
                    longitude: 4.89236,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Valence"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut-valence.fr/INFO.php",               	
                },  
                'lyon': {
                    latitude: 45.764043,
                    longitude: 4.835659,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Lyon"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://iuta.univ-lyon1.fr/informatique_lyon.htm",               	
                },  
                'aix': {
                    latitude: 43.529742,
                    longitude: 5.447427,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Aix en Provence"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut.univ-aix.fr/info/",               	
                },  
                'nice': {
                    latitude: 43.710173,
                    longitude: 7.261953,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Nice"
                        , position: "bottom"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.iut.unice.fr/site/index.cfm?cd=21210",               	
                },  
                'arles': {
                    latitude: 43.676647,
                    longitude: 4.627777,
                    attrs: {
                    	fill : "#7550d7"
                    },
                    text: {
                        content: "Arles"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    href: "http://www.up.univ-mrs.fr/iutarles/index.php?menu=smenu4_1",               	
                },                                 	
	        }
	    });