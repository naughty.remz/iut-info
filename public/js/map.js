        $(".container").mapael({
            map : {
                name : "france_departments",
                zoom : {
                    enabled: true,
                    touch: true
                },
                defaultArea: {
                    attrs :{
                        fill: "#ececec",
                        stroke: "#ececec"
                    },
                    attrsHover: {
                        fill: "#ececec"
                    }
                }
            },
            plots : {
                'Toulouse': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 43.600000,
                    longitude: 1.55,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Toulouse"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://iut.ups-tlse.fr",
                },
                'Lannion': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 48.732084,
                    longitude: -3.459144,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Lannion"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.iut-lannion.fr/lyceens-etudiants/choisir-un-dut/dut-informatique",
                },
                'vannes': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 47.658236,
                    longitude: -2.760847,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Vannes"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.iutvannes.fr/dut-informatique-morbihan-bretagne/",
                },
                'Nantes': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 47.218371,
                    longitude: -1.553621,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Nantes"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://iutnantes.univ-nantes.fr/formations/dut-bac-2-/dut-informatique-2019469.kjsp",
                },
                'rochelle': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 46.160329,
                    longitude: -1.151139,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "La Rochelle"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.iut-larochelle.fr/formations/dut/dut-informatique",
                },
                'bordeaux': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 44.837789,
                    longitude: -0.57918,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Bordeaux"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://www.iut.u-bordeaux.fr/info/",
                },
                'limoges': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 45.833619,
                    longitude: 1.261105,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Limoges"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.iut.unilim.fr/les-formations/dut/informatique/",
                },
                'blagnac': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 43.85087,
                    longitude: 1.39703,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Blagnac"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5",
                        fill: "#7550d7"
                    },
                    href: "https://www.iut-blagnac.fr/fr/",
                },
                'laval': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 48.078515,
                    longitude: -0.766991,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Laval"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.iut-laval.univ-lemans.fr/fr/info-informatique-et-programmation/dut-informatique.html",
                },
                'caen': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 49.182863,
                    longitude: -0.370679,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Caen"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://uniform.unicaen.fr/catalogue/formation/dut/5267-dut-informatique?s=iut-caen&r=1291042344184",
                },
                'havre': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 49.49437,
                    longitude: 0.107929,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Le Havre"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://www-iut.univ-lehavre.fr/index.php",
                },
                'calais': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 50.8,
                    longitude: 1.858686,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Calais"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.iut.univ-littoral.fr/wp/dut/info-informatique/",
                },
                'bayonne': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 43.492949,
                    longitude: -1.474841,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Bayonne"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://www.iutbayonne.univ-pau.fr/dut/informatique",
                },
                'lens': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 50.42893,
                    longitude: 2.83183,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Lens"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.iut-lens.univ-artois.fr/formations/offre-de-formation/dut/dut-informatique/",
                },
                'amiens': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 49.893567,
                    longitude: 2.295753,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Amiens"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.iut-amiens.fr/informatique/",
                },
                'paris': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 48.856614,
                    longitude: 2.352222,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Ile De France"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.idf.iut.fr/action/annuaire/dut/Informatique",
                },
                'orleans': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 47.902964,
                    longitude: 1.909251,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Orléans"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://www.univ-orleans.fr/iut-orleans/informatique",
                },
                'clermont': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 45.777222,
                    longitude: 3.087025,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Clermont-Ferrand"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://www.iut-clermont.fr/formations/dut-informatique/",
                },
                'puy': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 45.042768,
                    longitude: 3.882936,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Le Puy"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://ig.iut-clermont.fr/",
                },
                'rodez': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 44.349389,
                    longitude: 2.575986,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Rodez"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://www.iut-rodez.fr/fr/les-formations/dut-informatique/dut-informatique-presentation",
                },
                'montpellier': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 43.610769,
                    longitude: 3.876716,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Montpellier"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://iutdepinfo.iutmontp.univ-montp2.fr/",
                },
                'lille': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 50.62925,
                    longitude: 3.057256,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Lille"
                        , position: "right"
                        , margin: {x : 3, y: -3}
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.iut-a.univ-lille.fr/dut-info/",
                },
                'maubeuge': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 50.280228,
                    longitude: 3.9674,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Maubeuge"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://formations.uphf.fr/cdm/program/FR_RNE_0593279U_PR_I2INF261",
                },
                'reims': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 49.258329,
                    longitude: 4.031696,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Reims"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://iut-info.univ-reims.fr/",
                },
                'metz': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 49.119309,
                    longitude: 6.175716,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Metz"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://iut-metz.univ-lorraine.fr/dut-informatique",
                },
                'die': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 48.287468,
                    longitude: 6.947767,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "St Dié"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://iutsd.univ-lorraine.fr/",
                },
                'strasbourg': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 48.573355,
                    longitude: 7.752111,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Strasbourg"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://iutrs.unistra.fr/formation/dut/dut-informatique",
                },
                'belfort': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 47.639674,
                    longitude: 6.863849,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Belfort"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://www.iut-bm.univ-fcomte.fr/",
                },
                'nancy': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 48.692054,
                    longitude: 6.184417,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Nancy"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://iut-charlemagne.univ-lorraine.fr/informatique/dut-informatique/",
                },
                'dijon': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 47.322047,
                    longitude: 5.04148,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Dijon"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://iutdijon.u-bourgogne.fr/www/formations/dut/dut-informatique-iq-site-de-dijon.html",
                },
                'bourg': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 46.205167,
                    longitude: 5.225501,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Bourg en Bresse"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://iut.univ-lyon1.fr/formation/offre-de-formations/informatique-bourg-en-bresse-/informatique-bourg-en-bresse--790899.kjsp",
                },
                'annecy': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 45.899247,
                    longitude: 6.109384,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Annecy"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://www.iut-acy.univ-smb.fr/departement_info/le_departement_info/",
                },
                'grenoble': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 45.188529,
                    longitude: 5.724524,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Grenoble"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://iut2.univ-grenoble-alpes.fr/formations/les-departements-de-l-iut2/informatique-info--149104.kjsp",
                },
                'valence': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 44.933393,
                    longitude: 4.89236,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Valence"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://www.iut-valence.fr/nos-formations/dut/dut-informatique-168321.kjsp",
                },
                'lyon': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 45.763543,
                    longitude: 4.835659,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Lyon"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://iut.univ-lyon1.fr/formation/offre-de-formations/informatique-bourg-en-bresse-/dut-informatique-602485.kjsp",
                },
                'aix': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 43.529742,
                    longitude: 5.447427,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Aix en Provence"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "https://iut.univ-amu.fr/departements/informatique-info-aix",
                },
                'nice': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 43.710173,
                    longitude: 7.261953,
                    attrs: {
                        fill : "#7550d7"
                    },
                    text: {
                        content: "Nice"
                        , position: "bottom"
                        , margin: -10
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5"
                    },
                    href: "http://unice.fr/iut/presentation/accueil",
                },
                'arles': {
                    type: "image", target: "_blank",
                    url: "/img/marker.png",
                    width: 10,
                    height: 35,
                    latitude: 43.676647,
                    longitude: 4.627777,
                    attrs: {
                        fill : "#755d7"
                    },
                    text: {
                        content: "Arles"
                        , position: "top"
                        , attrs: {"font-size": 10, fill: "#7550d7", opacity: 0.6}
                        , attrsHover: {fill: "#7550d7", opacity: 1}
                    },
                    attrsHover: {
                        transform: "s1.5",
                    },
                    href: "https://iut.univ-amu.fr/sites/arles",
                },
            }
        });
